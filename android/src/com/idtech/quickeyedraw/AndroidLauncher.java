package com.idtech.quickeyedraw;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.widget.Toast;
import com.anjlab.android.iab.v3.BillingProcessor;
import com.anjlab.android.iab.v3.TransactionDetails;
import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import com.idtech.quickeyedraw.QEDGame;
import com.idtech.quickeyedraw.Tools.CommHandler;

public class AndroidLauncher extends AndroidApplication implements CommHandler, BillingProcessor.IBillingHandler {

	BillingProcessor bp;

	final private String[] productId = {"com.idtech.quickeyedraw.supportdev01",
			"com.idtech.quickeyedraw.supportdev02",
			"com.idtech.quickeyedraw.supportdev03"};

	final private String licenseKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAvpgXvLtPs4tqesRwQRfCBj1trQRLciTqgcw6+BEsWODGRRdMt1EphGJZWADM+DGcO+1SLUHPFtSXsdxco+7hf9ozJO+TaIsu/+U8SP+Um1z6bJFkuMFaw7X++wVYdz6BO4BnhaD6rm75NW0jpygGkWvTOFEBSJtLdmMGzQ6bw+ny10d7GAkTe3jeclOoc9xOZoFN2kzZhlrW9NuCHE7R7J3lyamio/r71zc5bfgM3cF2xHHjpMgxuVs6/Q2GDKlNeQeAVrwgsoIHrYYof8/ULBdift2VgYUx1kn7fLFMSrrDX+5LR9+uPWIKSK7TNON8ouz4prXHQ98ftNBmtz1DdwIDAQAB";

	private void showShareIntent(String msg) {
		Intent shareIntent = new Intent(Intent.ACTION_SEND);
		shareIntent.setType("text/plain");
		shareIntent.putExtra(Intent.EXTRA_SUBJECT, "\n\n");
		shareIntent.putExtra(Intent.EXTRA_TEXT, msg);
		startActivity(Intent.createChooser(shareIntent, getResources().getString(R.string.app_name)));
	}

	private void support(int id) {
		bp.purchase(AndroidLauncher.this, productId[id]);
	}

	Handler handler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			super.handleMessage(msg);

			String message = (String) msg.obj;

			switch (message) {
				case "SUPPORT01":
					support(0);
					break;
				case "SUPPORT02":
					support(1);
					break;
				case "SUPPORT03":
					support(2);
					break;
				default:
					showShareIntent(message);
					break;

			}

//			if (!message.equals("SUPPORT")) {
//				showShareIntent(message);
//			} else {
//				support();
//			}

		}
	};

	@Override
	protected void onCreate (Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();
		initialize(new QEDGame(this), config);

		bp = new BillingProcessor(this, licenseKey, this);
	}

	@Override
	public void showShare(String message) {
		Message stringMessage = Message.obtain();
		stringMessage.obj = message;
		handler.sendMessage(stringMessage);
	}


	@Override
	public void onProductPurchased(@NonNull String productId, @Nullable TransactionDetails details) {
		Toast.makeText(this, "HURRAY!", Toast.LENGTH_SHORT).show();
		bp.consumePurchase(productId);

	}

	@Override
	public void onPurchaseHistoryRestored() {

	}

	@Override
	public void onBillingError(int errorCode, @Nullable Throwable error) {

	}

	@Override
	public void onBillingInitialized() {

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (!bp.handleActivityResult(requestCode, resultCode, data))
			super.onActivityResult(requestCode, resultCode, data);
	}

	@Override
	protected void onDestroy() {
		if(bp != null)
			bp.release();

		super.onDestroy();
	}
}
