## QUICK EYE DRAW

Test your reflexes and play the fun, zany duck shooting gallery. Aim for the highest score. Get higher combos and increase the game run time.

No ducks were harmed in the making of this game :P

Try it out [here](https://drive.google.com/file/d/1yOc14hXrxCATp0QiI2JMcGpI6xOjOE4d/view?usp=sharing) using the APK in the link.

### SCREENSHOTS

#### **Main Menu**

<p align="center" ><img src="screenshots/a.png" alt="Main Menu" height="200"></p>

#### **Collection of in-app screenshots**

<p align="center" ><img src="screenshots/b.png" alt="Screenshot A" height="200"></p>
<br>
<p align="center" ><img src="screenshots/c.png" alt="Screenshot B" height="200"></p>
<br>
<p align="center" ><img src="screenshots/d.png" alt="Screenshot C" height="200"></p>

#### **Pause Screen**

<p align="center" ><img src="screenshots/e.png" alt="Pause Screen" height="200"></p>

### ATTRIBUTION

The visual assets were obtained from [Kenney's](kenney.nl) free [Shooting Gallery repository](https://kenney.nl/assets/shooting-gallery).

The music was [Batty McFaddin](https://incompetech.com/music/royalty-free/index.html?isrc=USUAN1200003) from Kevin MacLeod.
