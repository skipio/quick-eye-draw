package com.idtech.quickeyedraw.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.idtech.quickeyedraw.QEDGame;
import com.idtech.quickeyedraw.Tools.CommHandler;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.width = QEDGame.V_WIDTH;
		config.height = QEDGame.V_HEIGHT;

		class TestHandler implements CommHandler {

			@Override
			public void showShare(String message) {
				System.out.println(message);
			}
		}

		TestHandler handler = new TestHandler();

		new LwjglApplication(new QEDGame(handler), config);
	}


}
