package com.idtech.quickeyedraw.Entities;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Disposable;
import com.idtech.quickeyedraw.Screens.PlayScreen;

public class Duck extends Image implements Disposable {
    PlayScreen screen;

    Image stickImage;

    float bobY = 1;
    float newY;

    int score;

    float animatedeadspeed = -5f;

    boolean hitByPlayer;

//    Polygon polygon;

    Circle circle;

    boolean hit, changedTexture;

//    private static Texture[] duckTextureArray = {
//            new Texture("Objects/duck_outline_white.png"),
//            new Texture("Objects/duck_outline_brown.png"),
//            new Texture("Objects/duck_outline_yellow.png"),
//            new Texture("Objects/duck_outline_target_white.png"),
//            new Texture("Objects/duck_outline_target_brown.png"),
//            new Texture("Objects/duck_outline_target_yellow.png")};

//    private static Texture duckBackOutline = new Texture("Objects/duck_outline_back.png");

//    private static Texture stick = new Texture("Objects/stick_woodFixed_outline.png");
    private float speed;

    public Duck(PlayScreen screen, int colour, int target) {
        super(new Texture("Objects/duck" + String.valueOf(target*3 + colour) + ".png"));

        hitByPlayer = false;

        newY = .4f * screen.getHeight();

        this.screen = screen;

        score = 20 * (colour + 1);

        if (target == 0) score *= -1;

        speed = colour + 1;

        setPosition(- getWidth(), newY);

        screen.getStage().addActor(this);

        stickImage = new Image(new Texture("Objects/stick_woodFixed_outline.png"));
        updateStickImagePosition();

        hit = false;
        changedTexture = false;

        screen.getStage().addActor(stickImage);

        circle = new Circle();
        circle.setRadius(getWidth() / 2);

        updateCircle();
        //        polygon = new Polygon(new float[]{0, 39, 9, 16, 34, 2 , 63, 1, 89, 7, 99, 20, 102, 37, 95, 57, 106, 59, 113, 73, 109, 82, 100, 82, 99, 96, 81, 107, 63, 108, 47, 100, 43, 81, 50, 64, 31, 64, 25, 75, 9, 77, 2, 61});
//        polygon.setPosition(getX(), getY());

    }

    @Override
    public void act(float delta) {

        if (!hit) {
            newY = MathUtils.sin(stickImage.getX() / 30) * bobY;
            moveBy(screen.getGameSpeed() * speed * 100 * delta, newY);
            updateStickImagePosition();

            updateCircle();

            if (offScreen()) dispose();
        } else {
            hitDuck();
        }

    }

    private void hitDuck() {
        if (getScaleX() > 0) {
            animateDead();
        } else {
            if (!changedTexture) {
                setDrawable(new TextureRegionDrawable(new TextureRegion(new Texture("Objects/duck_outline_back.png"))));
                changedTexture = true;
            }
            if (getScaleX() <= -1) {
                if (getY() > 0) {
                    moveBy(0, animatedeadspeed);
                    stickImage.moveBy(0, animatedeadspeed);
                    animatedeadspeed -= .5f;
                } else {
                    remove();
                    stickImage.remove();
                    return;
                }

            } else {
                animateDead();
            }

        }
    }

    private void animateDead() {
        float oldWidth = getScaleX() * getWidth();
        scaleBy(-.25f, 0f);
        setScaleX(MathUtils.clamp(getScaleX(), -1, 1));
        float newWidth = getScaleX() * getWidth();
        moveBy((oldWidth - newWidth) / 2, 0f);
    }

    private void updateStickImagePosition() {
        stickImage.setPosition(getX() + getWidth() / 2 - stickImage.getWidth() / 2,
                getY() - stickImage.getHeight());
    }

    public Circle getBounds() {
        return circle;
    }

    private void updateCircle() {
        circle.setPosition(getX() + getWidth() / 2, getY() + getHeight() / 2);
    }

    @Override
    public void dispose() {
        hit = true;
        circle.setPosition(-400, -400);
        if (!hitByPlayer && !screen.getGameOverState() && score > 0 && !screen.timerOver()) {
            screen.changeTimer(-5);
            screen.timeLossPopup();
        }
        animateHit();

    }

    public void quietDispose() {
        hit = true;
        circle.setPosition(-400, -400);
        animateHit();
    }

    private void animateHit() {

    }

    public void setHitByPlayer() {
        hitByPlayer = true;
    }

    public boolean offScreen() {
        return  getX() > screen.getWidth();
    }

    public void fixStick() {
        stickImage.toFront();
    }

    public int getScore() {
        return score;
    }
}
