package com.idtech.quickeyedraw.Entities;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.Align;
import com.idtech.quickeyedraw.Screens.PlayScreen;

public class Crosshair extends Image {
    PlayScreen screen;
    Circle circle;

    public Crosshair(PlayScreen screen, Stage stage) {
        super(new Texture("HUD/crosshair_outline_large.png"));

        this.screen = screen;

        setPosition(screen.getWidth() / 2, screen.getHeight() / 2, Align.center);

        circle = new Circle();
        circle.setRadius(getWidth() / 4);
        circle.setPosition(getX() + getWidth() / 2,
                getY() + getHeight() / 2);

        addAction(Actions.alpha(0f));

        stage.addActor(this);



//        addListener(new InputListener() {
//            @Override
//            public boolean keyDown(InputEvent event, int keycode) {
//                if (keycode == Input.Keys.W)
//                    moveBy(0, moveVal);
//                if (keycode == Input.Keys.A)
//                    moveBy(-moveVal, 0);
//                if (keycode == Input.Keys.S)
//                    moveBy(0, -moveVal);
//                if (keycode == Input.Keys.D)
//                    moveBy(moveVal, 0);
//
//                return true;
//            }
//        });
    }

    @Override
    public void act(float delta) {
        super.act(delta);

        if (Gdx.input.isKeyPressed(Input.Keys.W))
            moveBy(0, 350 * delta);
        if (Gdx.input.isKeyPressed(Input.Keys.A))
            moveBy(- 350 * delta, 0);
        if (Gdx.input.isKeyPressed(Input.Keys.S))
            moveBy(0, -350 * delta);
        if (Gdx.input.isKeyPressed(Input.Keys.D))
            moveBy(350 * delta, 0);

        circle.setPosition(getX() + getWidth() / 2,
                getY() + getHeight() / 2);
    }

    public Circle getBounds() {
        return circle;
    }

    public void gameOver() {
        addAction(Actions.fadeOut(.5f));
    }

    public void gameStarted() {
        addAction(Actions.fadeIn(.5f));
        setPosition(screen.getWidth() / 2, screen.getHeight() / 2, Align.center);
        circle.setPosition(getX() + getWidth() / 2,
                getY() + getHeight() / 2);

    }
}
