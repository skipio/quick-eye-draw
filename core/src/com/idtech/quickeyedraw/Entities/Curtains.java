package com.idtech.quickeyedraw.Entities;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Array;
import com.idtech.quickeyedraw.Screens.PlayScreen;

public class Curtains {

    private Texture topCurtain = new Texture("Stall/curtain_top.png");
    private Texture sideCurtain = new Texture("Stall/curtain.png");
    PlayScreen screen;

    private Array<Image> topCurtainArray;

    private Image curtainLeft, curtainRight;
    private Image curtainRopeLeft, curtainRopeRight;
    private Image mainCurtainLeft, mainCurtainRight;
    private Image curtainsStriped;

    float topCurtainY;
    float topCurtainX;

    float ropeY;
    float ropeXExtra;
    float curtainSideY;

    float curtainDistanceX = sideCurtain.getWidth();

    public Curtains(PlayScreen screen, Stage stage) {
        this.screen = screen;

        topCurtainArray = new Array<Image>(5);

        topCurtainY = .8f * screen.getHeight();
        topCurtainX = .07f * screen.getWidth();

        for (int i = 0; i < 5; i++) {
            Image newCurtainTopImage = new Image(topCurtain);
            newCurtainTopImage.setPosition(topCurtainX, screen.getHeight());
//            newCurtainTopImage.setPosition(topCurtainX, topCurtainY);
            topCurtainX += topCurtain.getWidth() * .8f;
//            newCurtainTopImage.setPosition(
//                    screen.getWidth() / 2 + (i - 2) * topCurtain.getWidth() - topCurtain.getWidth() / 2, topCurtainY);

            topCurtainArray.add(newCurtainTopImage);
        }

        curtainLeft = new Image(sideCurtain);
        curtainRight = new Image(new TextureRegion(sideCurtain));
        ((TextureRegionDrawable)curtainRight.getDrawable()).getRegion().flip(true, false);

        curtainSideY = (screen.getHeight() - curtainLeft.getHeight()) / 2;

        curtainLeft.setPosition(-curtainDistanceX, curtainSideY);
        curtainRight.setPosition(screen.getWidth() + curtainDistanceX - curtainRight.getWidth(), curtainSideY);

        curtainRopeLeft = new Image(new Texture("Stall/curtain_rope.png"));
        curtainRopeRight = new Image(new Texture("Stall/curtain_rope.png"));


        ropeY = 255f;
        ropeXExtra = 5f;

        curtainRopeLeft.setPosition(-ropeXExtra - curtainDistanceX, ropeY);
        curtainRopeRight.setPosition(screen.getWidth() - curtainRopeRight.getWidth() + curtainDistanceX + ropeXExtra, ropeY);

        mainCurtainLeft = repeatImage(new Texture("Stall/bg_red.png"), 0, curtainSideY);
        mainCurtainLeft.setWidth(screen.getWidth() / 2);
        mainCurtainRight = repeatImage(new Texture("Stall/bg_red.png"), screen.getWidth() / 2, curtainSideY);

        for (Image image : topCurtainArray) {
            stage.addActor(image);
        }

        stage.addActor(curtainLeft);
        stage.addActor(curtainRight);

        stage.addActor(curtainRopeLeft);
        stage.addActor(curtainRopeRight);

        stage.addActor(mainCurtainLeft);
        stage.addActor(mainCurtainRight);
    }

    public void toFront() {
        for (Image image : topCurtainArray) image.toFront();
        mainCurtainLeft.toFront();
        mainCurtainRight.toFront();
        curtainLeft.toFront();
        curtainRight.toFront();
        curtainRopeLeft.toFront();
        curtainRopeRight.toFront();
    }

    public void open() {
        Action moveCurtains = new Action() {
            @Override
            public boolean act(float delta) {
                for (Image image: topCurtainArray) image.addAction(Actions.moveTo(image.getX(), topCurtainY, .5f, Interpolation.pow2Out));
                return true;
            }
        };

        mainCurtainLeft.addAction(
                Actions.sequence(
                        Actions.moveTo(-mainCurtainLeft.getWidth(), curtainSideY, 1f, Interpolation.fade),
                        Actions.parallel(
                                Actions.addAction(Actions.moveTo(0, curtainSideY, .5f, Interpolation.pow2Out), curtainLeft),
                                Actions.addAction(Actions.moveTo(-ropeXExtra, ropeY, .5f, Interpolation.pow2Out), curtainRopeLeft),
                                moveCurtains)));

        mainCurtainRight.addAction(
                Actions.sequence(
                        Actions.moveTo(screen.getWidth(), curtainSideY, 1f, Interpolation.fade),
                        Actions.parallel(
                                Actions.addAction(Actions.moveTo(screen.getWidth() - curtainRight.getWidth(), curtainSideY, .5f, Interpolation.pow2Out), curtainRight),
                                Actions.addAction(Actions.moveTo(screen.getWidth() - curtainRopeRight.getWidth() + ropeXExtra, ropeY, .5f, Interpolation.pow2Out), curtainRopeRight))));
    }

    public void close() {
        Action moveCurtains = new Action() {
            @Override
            public boolean act(float delta) {
                for (Image image: topCurtainArray) image.addAction(Actions.moveTo(image.getX(), screen.getHeight(), .5f, Interpolation.pow2Out));
                return true;
            }
        };

        curtainLeft.addAction(
                Actions.sequence(
                        Actions.parallel(
                                Actions.addAction(Actions.moveTo(-curtainDistanceX, curtainSideY, .5f, Interpolation.pow2Out)),
                                Actions.addAction(Actions.moveTo(-ropeXExtra - curtainDistanceX, ropeY, .5f, Interpolation.pow2Out), curtainRopeLeft),
                                moveCurtains),
                        Actions.addAction(Actions.moveTo(0, curtainSideY, 1f, Interpolation.fade), mainCurtainLeft)));

//        curtainLeft.addAction(
//                Actions.sequence(
//                        Actions.parallel(
//                                Actions.moveTo(-curtainDistanceX, curtainSideY, .5f, Interpolation.pow2Out),
//                                Actions.addAction(Actions.moveTo(-ropeXExtra - curtainDistanceX, ropeY, .5f, Interpolation.pow2Out), curtainRopeLeft),
//                                moveCurtains),
//                        Actions.addAction(Actions.moveTo(0, curtainSideY, 1f, Interpolation.fade), mainCurtainLeft)));

        curtainRight.addAction(
                Actions.sequence(
                        Actions.parallel(
                                Actions.addAction(Actions.moveTo(screen.getWidth() - curtainRight.getWidth() + curtainDistanceX, curtainSideY, .5f, Interpolation.pow2Out)),
                                Actions.addAction(Actions.moveTo(screen.getWidth() - curtainRopeRight.getWidth() + ropeXExtra + curtainDistanceX, ropeY, .5f, Interpolation.pow2Out), curtainRopeRight)),
                        Actions.addAction(Actions.moveTo(screen.getWidth() / 2, curtainSideY, 1f, Interpolation.fade), mainCurtainRight)));
    }

    private Image repeatImage(Texture texture, float positionX, float positionY) {
        texture.setWrap(Texture.TextureWrap.Repeat, Texture.TextureWrap.Repeat);
        TextureRegion repeatRegion = new TextureRegion(texture,
                0, 0, screen.getWidth(), screen.getHeight());
        repeatRegion.setRegionWidth((int)screen.getWidth() / 2);

        Image repeat = new Image(repeatRegion);
        repeat.setPosition(positionX, positionY);

        return repeat;
    }


}
