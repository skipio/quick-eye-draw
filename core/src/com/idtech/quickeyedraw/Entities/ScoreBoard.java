package com.idtech.quickeyedraw.Entities;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Disposable;
import com.idtech.quickeyedraw.Screens.PlayScreen;
import com.sun.org.apache.xml.internal.utils.XMLStringFactory;

public class ScoreBoard implements Disposable {
    PlayScreen screen;
    Stage stage;

    int score;

    Image[] mainScore;

    Label comboScore;

    float[] scorePositions;

    Music gameMusic;

    Sound gameEnd;

    Image scoreLabel;

    int combo, maxCombo;

    public ScoreBoard(PlayScreen screen, Stage stage) {
        this.screen = screen;

        this.stage = stage;

//        gameMusic = Gdx.audio.newMusic(Gdx.files.internal("Music/Batty McFaddin.mp3"));
        gameMusic = screen.getManager().get("Music/Batty McFaddin.mp3");
        gameMusic.setLooping(true);

//        gameEnd = Gdx.audio.newSound(Gdx.files.internal("Sounds/tadaa.wav"));
        gameEnd = screen.getManager().get("Sounds/tadaa.wav");

        mainScore = new Image[7];
        scorePositions = new float[7];

        for (int i = 0, mainScoreLength = mainScore.length; i < mainScoreLength; i++) {
            mainScore[i] = new Image(new TextureRegionDrawable(new TextureRegion(screen.transparentTexture)));
            mainScore[i].setPosition((6 - i) * 48 + 10, .88f * screen.getHeight());
            scorePositions[i] = mainScore[i].getX();
        }

        combo = 0;
        maxCombo = 0;

        comboScore = new Label("x " + String.valueOf(combo), new Label.LabelStyle(
                new BitmapFont(Gdx.files.internal("HUD/luckiestguy.fnt")), Color.WHITE
        ));

        comboScore.setOrigin(Align.center);
        comboScore.setPosition(.75f * screen.getWidth(), .88f * screen.getHeight());
        comboScore.setVisible(false);

        updateScoreBoard();

        toStage();

        score = 0;

        scoreLabel = new Image(new Texture("HUD/text_score.png"));
        scoreLabel.setPosition(-scoreLabel.getWidth(), .5f * screen.getHeight());
        scoreLabel.addAction(Actions.alpha(0f));
        stage.addActor(scoreLabel);
        stage.addActor(comboScore);
    }

    private void toStage() {
        for (Image image : mainScore) {
            image.setAlign(Align.center);
            stage.addActor(image);
        }

        setVisibility(false);
    }

    public void setVisibility(boolean isVisible) {
        int a = isVisible ? 1 : 0;

        for (Image image: mainScore) {
            image.addAction(Actions.alpha(a));
        }

    }

    public void toFront() {
        for (Image image : mainScore) {
            image.toFront();
        }
        comboScore.toFront();
        scoreLabel.toFront();
    }

    public void updateScoreBoard() {
        boolean negScore = false;
        if (score < 0) {
            score *= -1;
            negScore = true;
        }
        String scoreVal = String.valueOf(score);
//        for (int i = scoreVal.length() - 1; i >= 0; i--) {
//            ((TextureRegionDrawable)mainScore[(scoreVal.length() - i - 1)].getDrawable())
//                    .getRegion().setTexture(PlayScreen.numTextures[Character.getNumericValue(scoreVal.charAt(i))]);
//        }

        for (int i = 0; i < 7; i++) {
            if (i <= scoreVal.length() - 1)
                ((TextureRegionDrawable)mainScore[i].getDrawable())
                    .getRegion().setTexture(screen.numTextures[Character.getNumericValue(scoreVal.charAt(scoreVal.length() - i - 1))]);
            else
                ((TextureRegionDrawable)mainScore[i].getDrawable())
                        .getRegion().setTexture(screen.transparentTexture);
        }

        if (negScore) {
            ((TextureRegionDrawable)mainScore[scoreVal.length()].getDrawable())
                    .getRegion().setTexture(screen.numTextures[13]);
        }
//        else {
//            ((TextureRegionDrawable)mainScore[scoreVal.length()].getDrawable())
//                    .getRegion().setTexture(PlayScreen.transparentTexture);
//        }

        for (int i = 0, mainScoreLength = mainScore.length; i < mainScoreLength; i++) {
            Image image = mainScore[i];
            Texture texture = ((TextureRegionDrawable) image.getDrawable()).getRegion().getTexture();
            image.setSize(texture.getWidth(), texture.getHeight());
            image.setX(scorePositions[i] + (48 - image.getWidth()) / 2);
        }
    }

    public void gameOver() {
        gameMusic.stop();

        if (screen.getTimer().getTimer() == 0)
            gameEnd.play(screen.getMusicAmp());

        // Updating the max Combo value
        comboScore.setVisible(false);
        screen.getMenu().updateCombo(maxCombo);
        screen.getMenu().updateScore(score);

        boolean negScore = false;
        if (score < 0) {
            score *= -1;
            negScore = true;
        }
        String scoreVal = String.valueOf(score);

        int val = scoreVal.length();
        if (negScore) val++;

        float distanceToMoveX = screen.getWidth() / 2 - mainScore[(val - 1)].getX();


        for (Image image: mainScore) {
            image.addAction(Actions.moveTo(image.getX() + distanceToMoveX, .48f * screen.getHeight(), .5f, Interpolation.pow2Out));
        }

        scoreLabel.addAction(Actions.parallel(
                                Actions.fadeIn(.5f, Interpolation.fade),
                                Actions.moveTo(screen.getWidth()  * .45f - scoreLabel.getWidth(), .48f * screen.getHeight(), .5f, Interpolation.exp5)));
    }

    public void gameStarted() {
        setVisibility(true);

        gameMusic.play();
        gameMusic.setVolume(screen.getMusicAmp());

//        updateScoreBoard();

        resetScoreBoard();
    }

    private void resetScoreBoard() {
        refreshScore();

//        for (int i = 0, mainScoreLength = mainScore.length; i < mainScoreLength; i++) {
//            mainScore[i].addAction(Actions.sequence(
//                    Actions.moveTo(scorePositions[i], .88f * screen.getHeight(), .5f, Interpolation.fade)
//            ));
//        }
//
//        updateScoreBoard();

        scoreLabel.addAction(Actions.parallel(
                Actions.moveTo(-scoreLabel.getWidth(), .5f * screen.getHeight(), .5f, Interpolation.exp5Out),
                Actions.sequence(
                        new Action() {
                            @Override
                            public boolean act(float delta) {
                                for (int i = 0, mainScoreLength = mainScore.length; i < mainScoreLength; i++) {
                                    mainScore[i].addAction(Actions.sequence(
                                            Actions.moveTo(scorePositions[i], .88f * screen.getHeight(), .5f, Interpolation.fade)
                                    ));
                                }
                                return true;
                            }
                        },
                        new Action() {
                            @Override
                            public boolean act(float delta) {
                                updateScoreBoard();
                                return true;
                            }
                        }
                )));
    }

    public void toMenu() {
        resetScoreBoard();

        setVisibility(false);
    }

    public void refreshScore() {
        score = 0;
    }

    public void addScore(int score) {
        this.score += score;
    }

    public int getScore() {
        return score;
    }

    @Override
    public void dispose() {
//        gameMusic.dispose();
//        gameEnd.dispose();
    }

    public void updateComboScore(boolean hit) {
        if (hit) {
            combo++;

            if (combo > maxCombo) maxCombo = combo;

            if (combo > 1) {
                comboScore.setText("X " + String.valueOf(combo));
                comboScore.setVisible(true);
            }

            if (combo > 10) {
                screen.getTimer().changeTime(2);
            }
        } else {
            combo = 0;
            comboScore.setVisible(false);
        }
    }
}
