package com.idtech.quickeyedraw.Entities;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.MoveToAction;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.Disposable;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.idtech.quickeyedraw.QEDGame;
import com.idtech.quickeyedraw.Screens.PlayScreen;

public class DPad implements Disposable {
    private Stage stage;
    Viewport viewport;

    PlayScreen screen;
    Texture inner, outer;
    Image outer_image, inner_image;
    float inner_x, inner_y;
    float x1, x2, y1, y2;

    float drag_originX, drag_originY;
    boolean dragStarted;

    public DPad(PlayScreen screen, SpriteBatch spriteBatch) {
        this.screen = screen;

        viewport = new FitViewport(QEDGame.V_WIDTH, QEDGame.V_HEIGHT, new OrthographicCamera());
        stage = new Stage(viewport, spriteBatch);

        inner = new Texture("HUD/dpad_inner.png");
        outer = new Texture("HUD/dpad_outer.png");

        outer_image = new Image(outer);

        inner_image = new Image(inner);

        drag_originX = 0;
        drag_originY = 0;

        dragStarted = false;

        defineDragDPad();

        stage.addActor(outer_image);
        stage.addActor(inner_image);

        Gdx.input.setInputProcessor(stage);

    }

    private void defineDragDPad() {
        outer_image.setBounds(50, 50, outer.getWidth(), outer.getHeight());
        inner_image.setBounds((outer.getWidth() - inner.getWidth()) / 2 + outer_image.getX(),
                (outer.getHeight() - inner.getHeight()) / 2 + outer_image.getY(),
                inner.getWidth(), inner.getHeight());

        inner_x = inner_image.getX();
        inner_y = inner_image.getY();

        x1 = 0;
        y1 = 0;

        outer_image.setTouchable(Touchable.enabled);
        inner_image.setTouchable(Touchable.disabled);

        InputListener listener = new InputListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }

            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                dragStarted = false;
                MoveToAction move = new MoveToAction();
                move.setPosition((outer.getWidth() - inner.getWidth()) / 2 + outer_image.getX(),
                        (outer.getHeight() - inner.getHeight()) / 2 + outer_image.getY());
                move.setDuration(.1f);
                inner_image.addAction(move);
                x1 = 0;
                y1 = 0;
            }

            @Override
            public void touchDragged(InputEvent event, float x, float y, int pointer) {
                if (!dragStarted) {
                    drag_originX = x;
                    drag_originY = y;
                    dragStarted = true;
                } else {
                    float deltaX = x - drag_originX;
                    float mulX = outer_image.getWidth() / 2;
                    float deltaY = y - drag_originY;
                    float mulY = outer_image.getHeight() / 2;

                    // Used sigmoid function to limit the change in position of the dpad
                    float moveX = ((float) (1 / (1 + Math.exp(-1 * deltaX / 100))) - 0.5f);
//                    x2 = moveX;
                    x2 = deltaX;
                    float moveY = ((float) (1 / (1 + Math.exp(-1 * deltaY / 100))) - 0.5f);
//                    y2 = moveY;
                    y2 = deltaY;

                    float delX = x2 - x1;
                    float delY = y2 - y1;
                    x1 = x2;
                    y1 = y2;

                    MoveToAction move = new MoveToAction();
                    move.setPosition(moveX * mulX + inner_x, moveY * mulY + inner_y);
//                    Gdx.app.log("Movement", String.valueOf(inner_image.getX()) + ", " + String.valueOf(inner_image.getY()));
                    inner_image.addAction(move);

                    screen.moveCrossHair(delX, delY);
                }
            }
        };

        outer_image.addListener(listener);

        outer_image.setTouchable(Touchable.disabled);
        outer_image.addAction(Actions.alpha(0));
        inner_image.addAction(Actions.alpha(0));
    }

    @Override
    public void dispose() {
        inner.dispose();
        outer.dispose();
        stage.dispose();
    }

//    public void addStage(Stage stage) {
//        stage.addActor(outer_image);
//        stage.addActor(inner_image);
//    }

    public void toFront() {
        outer_image.toFront();
        inner_image.toFront();
    }

    public Stage getStage() {
        return stage;
    }

    public void setTouchable(Touchable touchable) {
        outer_image.setTouchable(touchable);
    }

    public void gameOver() {
        outer_image.setTouchable(Touchable.disabled);
        outer_image.addAction(Actions.fadeOut(.5f));
        inner_image.addAction(Actions.fadeOut(.5f));
    }

    public void gameStarted() {
        outer_image.setTouchable(Touchable.enabled);
        outer_image.addAction(Actions.fadeIn(.5f));
        inner_image.addAction(Actions.fadeIn(.5f));
    }
}
