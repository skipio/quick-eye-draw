package com.idtech.quickeyedraw.Entities;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;

public class PopupScore extends Label {

//    private static LabelStyle style = new Label.LabelStyle(
//            new BitmapFont(Gdx.files.internal("HUD/luckiestguybordersmall.fnt")), Color.WHITE);



    // Will receive location of center of top of object that has been hit
    public PopupScore(Stage stage, int score, float positionX, float positionY) {
        this(stage, (score > 0 ? "+" : "-") + String.valueOf(Math.abs(score)), positionX, positionY);
    }

    private PopupScore(Stage stage, String string, float positionX, float positionY) {
        super(string, new Label.LabelStyle(
                new BitmapFont(Gdx.files.internal("HUD/luckiestguybordersmall.fnt")), Color.WHITE));

        setPosition(positionX - getWidth() / 2, positionY);

        stage.addActor(this);

        float duration = .6f;

        addAction(Actions.sequence(
                Actions.alpha(0),
                Actions.parallel(
                        Actions.moveBy(0, 10, duration, Interpolation.fade),
                        Actions.fadeIn(duration)
                ),
                Actions.fadeOut(duration*.8f),
                Actions.removeActor()
        ));
    }

    public PopupScore(Stage dpadStage, float v, float v1) {
        this(dpadStage, "-5s", v, v1);
    }
}
