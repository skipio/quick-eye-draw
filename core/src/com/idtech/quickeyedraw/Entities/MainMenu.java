package com.idtech.quickeyedraw.Entities;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ImageTextButton;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Slider;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.Window;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.NinePatchDrawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Disposable;
import com.idtech.quickeyedraw.Screens.PlayScreen;

public class MainMenu implements Disposable {
    private int highCombo;
    private Label quick, eye, draw;
    private TextButton newGame, settings, howToPlay, credits;

    Button exitButton;

    private Window howToPlayWindow, creditsWindow, settingsWindow;

    private WrappedImageTextButton support;

    PlayScreen screen;
    Stage stage;

    Sound menuFire, windowOpen, windowClose, duckFire, targetFire, missFire;

    Music menuMusic;

    long id;

    int highScore;

    WrappedLabel highScoreLabel, highScoreVal, highComboLabel, highComboVal, supportDevLabel;

    boolean showingSupport;

    SupportButton button1, button3, button7;

    private TextButton menuButton(String name, String buttonColor) {
        TextButton.TextButtonStyle style = new TextButton.TextButtonStyle(
                new NinePatchDrawable(new NinePatch(new Texture("HUD/" + buttonColor + "Up.png"), 7, 7, 7, 9)),
                new NinePatchDrawable(new NinePatch(new Texture("HUD/" + buttonColor + "Down.png"), 7, 7, 10, 6)),
                new NinePatchDrawable(new NinePatch(new Texture("HUD/" + buttonColor + "Down.png"), 7, 7, 10, 6)),
                new BitmapFont(Gdx.files.internal("HUD/luckiestguymed.fnt"))
        );

        return new TextButton(" " + name + " ", style);
    }

    private Label logoLabel(String name, Color fontColor) {
        Label.LabelStyle style = new Label.LabelStyle(
                new BitmapFont(Gdx.files.internal("HUD/luckiestguyborder.fnt")), fontColor);

        return new Label(name, style);
    }

    public MainMenu(PlayScreen screen, Stage stage) {
        this.screen = screen;
        this.stage = stage;

        support = new WrappedImageTextButton(screen);

        support.setPosition(-2*support.getWidth(), .35f * screen.getHeight());
        support.setScale(0f, 0f);
        support.centerOrigin();

//        menuFire = Gdx.audio.newSound(Gdx.files.internal("Sounds/gunfire.wav"));
//        windowOpen = Gdx.audio.newSound(Gdx.files.internal("Sounds/windowOpen.ogg"));
//        windowClose = Gdx.audio.newSound(Gdx.files.internal("Sounds/windowClose.ogg"));

        menuFire = screen.getManager().get("Sounds/gunfire.wav");
        windowOpen = screen.getManager().get("Sounds/windowOpen.ogg");
        windowClose = screen.getManager().get("Sounds/windowClose.ogg");
        duckFire = screen.getManager().get("Sounds/hitDuck.mp3");
        targetFire = screen.getManager().get("Sounds/hitTarget.wav");
        missFire = screen.getManager().get("Sounds/failhit.ogg");

//        menuMusic = Gdx.audio.newMusic(Gdx.files.internal("Music/sillycircus.ogg"));
        menuMusic = screen.getManager().get("Music/sillycircus.ogg");
        menuMusic.setLooping(true);

        highScore = screen.getPresentHighScore();

        resetHighScoreVal();

        highScoreLabel = new WrappedLabel("HIGH SCORE", false);
        highScoreLabel.centerOrigin();
        highScoreLabel.setPosition(screen.getWidth(), highScoreVal.getY() + 5 + highScoreVal.getHeight());
        highScoreLabel.setScale(0f, 0f);

        highCombo = screen.getPresentMaxCombo();

        resetHighComboVal();

        highComboLabel = new WrappedLabel("MAX COMBO", false);
        highComboLabel.centerOrigin();
        highComboLabel.setScale(0f, 0f);
        highComboLabel.setPosition(screen.getWidth(), highComboVal.getY() + 5 + highComboVal.getHeight());


        quick = logoLabel("QUICK", Color.RED);
        eye = logoLabel("EYE", Color.GREEN);
        draw = logoLabel("DRAW", Color.BLUE);

        newGame = menuButton("NEW GAME", "green");
        settings = menuButton("SETTINGS", "blue");
        howToPlay = menuButton("HOW TO PLAY", "yellow");
        credits = menuButton("CREDITS", "grey");

        quick.setPosition(-quick.getWidth(), screen.getHeight() - quick.getHeight() - 20);
        eye.setPosition(-quick.getWidth(), screen.getHeight() - 1.2f * eye.getHeight() - 20);
        draw.setPosition(-quick.getWidth(), screen.getHeight() -  1.4f * draw.getHeight() - 20);

        credits.setPosition(-credits.getWidth(), .15f * screen.getHeight());
        howToPlay.setPosition(-howToPlay.getWidth(), credits.getY() + credits.getHeight() + 20);
        settings.setPosition(screen.getWidth(), howToPlay.getY() + howToPlay.getHeight() + 20);
        newGame.setPosition(-newGame.getWidth(), settings.getY() + settings.getHeight() + 20);

        exitButton = new Button(new TextureRegionDrawable(new TextureRegion(new Texture("HUD/exitUp.png"))),
                new TextureRegionDrawable(new TextureRegion(new Texture("HUD/exitDown.png"))));
        exitButton.setPosition(2*screen.getWidth(), screen.getHeight() - 15 - exitButton.getHeight());

        supportDevLabel = new WrappedLabel("SUPPORT THE DEV", false);
        supportDevLabel.centerOrigin();
        supportDevLabel.setPosition(-2*support.getWidth(), .35f * screen.getHeight());
        supportDevLabel.setScale(0f, 0f);
        supportDevLabel.addAction(Actions.alpha(0f));

        showingSupport = false;

        // Setup support buttons
        button1 = new SupportButton(screen, "1", "01");
        button1.centerOrigin();
        button1.setPosition(-2*support.getWidth(), .35f * screen.getHeight());
        button1.setScale(0f, 0f);
        button1.addAction(Actions.alpha(0f));

        button1.addAction(Actions.after(Actions.forever(Actions.sequence(
                Actions.scaleBy(.2f, .2f, .05f),
                Actions.scaleBy(-.2f, -.2f, .05f),
                Actions.delay(1.9f)
        ))));

        button3 = new SupportButton(screen, "3", "02");
        button3.centerOrigin();
        button3.setPosition(-2*support.getWidth(), .35f * screen.getHeight());
        button3.setScale(0f, 0f);
        button3.addAction(Actions.alpha(0f));

        button3.addAction(Actions.after(Actions.forever(Actions.sequence(
                Actions.scaleBy(.2f, .2f, .05f),
                Actions.scaleBy(-.2f, -.2f, .05f),
                Actions.delay(.9f)
        ))));

        button7 = new SupportButton(screen, "7", "03");
        button7.centerOrigin();
        button7.setPosition(-2*support.getWidth(), .35f * screen.getHeight());
        button7.setScale(0f, 0f);
        button7.addAction(Actions.alpha(0f));

        button7.addAction(Actions.after(Actions.forever(Actions.sequence(
                Actions.scaleBy(.2f, .2f, .05f),
                Actions.scaleBy(-.2f, -.2f, .05f),
                Actions.delay(.4f)
        ))));


        setUpSettings();
        setUpHowtoPlay();
        setUpCredits();

        setUpListeners();

        addToStage();
    }

    public void resetHighComboVal() {
        highComboVal = new WrappedLabel("x" + String.valueOf(highCombo), true);
        highComboVal.centerOrigin();
        highComboVal.setScale(0f, 0f);
        highComboVal.setPosition(screen.getWidth(), .25f * screen.getHeight());
    }

    public void resetHighScoreVal() {
        highScoreVal = new WrappedLabel(String.valueOf(highScore), true);
        highScoreVal.centerOrigin();
        highScoreVal.setPosition(screen.getWidth(), .5f * screen.getHeight());
        highScoreVal.setScale(0f, 0f);
    }

    private void setUpListeners() {
        exitButton.addListener(new ClickListener() {
            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                Gdx.app.exit();
            }
        });

        newGame.addListener(new InputListener(){
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                newGame.setChecked(true);


                return true;
            }

            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                super.touchUp(event, x, y, pointer, button);

                newGame.setChecked(false);

                leaveMainMenu();

                screen.newGame();
            }
        });

        settings.addListener(new InputListener(){
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                settings.setChecked(true);

                return true;
            }

            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                super.touchUp(event, x, y, pointer, button);

                settings.setChecked(false);

                id = windowOpen.play(.5f * screen.getSoundAmp());

                settingsWindow.addAction(Actions.parallel(
                        Actions.moveBy(- settingsWindow.getWidth() / 2, - settingsWindow.getHeight() / 2, .3f, Interpolation.exp10Out),
                        Actions.scaleTo(1f, 1f, .3f, Interpolation.exp10Out),
                        Actions.fadeIn(.3f, Interpolation.exp10Out)));

                stage.addActor(settingsWindow);
            }
        });

        howToPlay.addListener(new InputListener(){
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                howToPlay.setChecked(true);

                return true;
            }

            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                super.touchUp(event, x, y, pointer, button);

                howToPlay.setChecked(false);

                id = windowOpen.play(.5f * screen.getSoundAmp());

                howToPlayWindow.addAction(Actions.parallel(
                        Actions.moveBy(- howToPlayWindow.getWidth() / 2, - howToPlayWindow.getHeight() / 2, .3f, Interpolation.exp10Out),
                        Actions.scaleTo(1f, 1f, .3f, Interpolation.exp10Out),
                        Actions.fadeIn(.3f, Interpolation.exp10Out)));

                stage.addActor(howToPlayWindow);
            }


        });

        credits.addListener(new InputListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                credits.setChecked(true);

                return true;
            }

            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                super.touchUp(event, x, y, pointer, button);

                credits.setChecked(false);

                id = windowOpen.play(.5f * screen.getSoundAmp());

                creditsWindow.addAction(Actions.parallel(
                        Actions.moveBy(- creditsWindow.getWidth() / 2, - creditsWindow.getHeight() / 2, .3f, Interpolation.exp10Out),
                        Actions.scaleTo(1f, 1f, .3f, Interpolation.exp10Out),
                        Actions.fadeIn(.3f, Interpolation.exp10Out)));

                stage.addActor(creditsWindow);
            }
        });

    }

    private void setUpSettings() {
        settingsWindow = new Window("SETTINGS", new Window.WindowStyle(
                new BitmapFont(Gdx.files.internal("HUD/luckiestguyborder.fnt")),
                Color.LIGHT_GRAY,
                new NinePatchDrawable(new NinePatch(new Texture("HUD/grey_panel.png"), 7, 7, 7, 7))
        ));

        settingsWindow.getTitleLabel().setAlignment(Align.center);
        settingsWindow.setKeepWithinStage(false);

        settingsWindow.padTop(64);
//                settingsWindow.pack();
        settingsWindow.setBounds(.2f * screen.getWidth(), .15f * screen.getHeight(),
                .6f * screen.getWidth(), .7f * screen.getHeight());
        settingsWindow.setMovable(false);

        Label soundLabel = new Label("SOUND", new Label.LabelStyle(
                new BitmapFont(Gdx.files.internal("HUD/luckiestguysmall.fnt")), Color.LIGHT_GRAY
        ));

        final Slider soundSlider = new Slider(0f, 1f, .2f, false, new Slider.SliderStyle(
                new TextureRegionDrawable(new TextureRegion(new Texture("HUD/grey_sliderHorizontal.png"))),
                new TextureRegionDrawable(new TextureRegion(new Texture("HUD/blue_circle.png")))
        ));

        soundSlider.setValue(screen.getSoundAmp());
        soundSlider.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                screen.setSoundAmp(soundSlider.getValue());
                menuFire.play(screen.getSoundAmp() * .1f);
            }
        });

        Image soundOff = new Image(new Texture("HUD/audioOff.png"));
        Image soundOn = new Image(new Texture("HUD/audioOn.png"));

        soundOff.setColor(Color.DARK_GRAY);
        soundOn.setColor(Color.DARK_GRAY);

        settingsWindow.add(soundLabel).padTop(15).padBottom(15);
        settingsWindow.add(soundOff);
        settingsWindow.add(soundSlider).expandX().fillX().width(200);
        settingsWindow.add(soundOn).row();

        Label musicLabel = new Label("MUSIC", new Label.LabelStyle(
                new BitmapFont(Gdx.files.internal("HUD/luckiestguysmall.fnt")), Color.LIGHT_GRAY
        ));

        final Slider musicSlider = new Slider(0f, 1f, .2f, false, new Slider.SliderStyle(
                new TextureRegionDrawable(new TextureRegion(new Texture("HUD/grey_sliderHorizontal.png"))),
                new TextureRegionDrawable(new TextureRegion(new Texture("HUD/blue_circle.png")))
        ));

        musicSlider.setValue(screen.getMusicAmp());
        musicSlider.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                screen.setMusicAmp(musicSlider.getValue());
                menuMusic.setVolume(screen.getMusicAmp());
            }
        });

        Image musicOff = new Image(new Texture("HUD/musicOff.png"));
        Image musicOn = new Image(new Texture("HUD/musicOn.png"));

        musicOff.setColor(Color.DARK_GRAY);
        musicOn.setColor(Color.DARK_GRAY);

        settingsWindow.add(musicLabel).padTop(15).padBottom(15);
        settingsWindow.add(musicOff);
        settingsWindow.add(musicSlider).expandX().fillX().width(200);
        settingsWindow.add(musicOn).row();

        Label sensitivityLabel = new Label("CURSOR\nSENSITIVITY", new Label.LabelStyle(
                new BitmapFont(Gdx.files.internal("HUD/luckiestguysmall.fnt")), Color.LIGHT_GRAY
        ));

        sensitivityLabel.setAlignment(Align.center);

        final Slider sensitivitySlider = new Slider(20f, 100f, 1f, false, new Slider.SliderStyle(
                new TextureRegionDrawable(new TextureRegion(new Texture("HUD/grey_sliderHorizontal.png"))),
                new TextureRegionDrawable(new TextureRegion(new Texture("HUD/blue_circle.png")))
        ));

        sensitivitySlider.setValue(screen.getCursorSensitivity());
        sensitivitySlider.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                screen.setCursorSensitivity(sensitivitySlider.getValue());
            }
        });

        Label zeroLabel = new Label("LOW", new Label.LabelStyle(
                new BitmapFont(Gdx.files.internal("HUD/luckiestguysmall.fnt")), Color.DARK_GRAY
        ));

        Label hundredLabel = new Label("HIGH", new Label.LabelStyle(
                new BitmapFont(Gdx.files.internal("HUD/luckiestguysmall.fnt")), Color.DARK_GRAY
        ));

        settingsWindow.add(sensitivityLabel).padTop(15).padBottom(15).padLeft(10);
        settingsWindow.add(zeroLabel).padLeft(10).padRight(10);
        settingsWindow.add(sensitivitySlider).fillX().expandX().width(200);
        settingsWindow.add(hundredLabel).padLeft(10).padRight(10).row();

        final TextButton dismissSettings = new TextButton("CLOSE", new TextButton.TextButtonStyle(
                new NinePatchDrawable(new NinePatch(new Texture("HUD/blueUp.png"), 7, 7, 7, 9)),
                new NinePatchDrawable(new NinePatch(new Texture("HUD/blueDown.png"), 7, 7, 10, 6)),
                new NinePatchDrawable(new NinePatch(new Texture("HUD/blueDown.png"), 7, 7, 10, 6)),
                new BitmapFont(Gdx.files.internal("HUD/luckiestguymed.fnt"))
        ));

        dismissSettings.addListener(new ClickListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                dismissSettings.setChecked(true);

                return true;
            }

            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                super.touchUp(event, x, y, pointer, button);

                dismissSettings.setChecked(false);

                id = windowClose.play(.5f * screen.getSoundAmp());

                settingsWindow.addAction(Actions.sequence(
                        Actions.parallel(
                                Actions.moveBy(settingsWindow.getWidth() / 2, settingsWindow.getHeight() / 2, .3f, Interpolation.exp10In),
                                Actions.scaleTo(0f, 0f, .3f, Interpolation.exp10In),
                                Actions.fadeOut(.3f, Interpolation.fade)),
                        Actions.removeActor()));
            }
        });

        settingsWindow.add(dismissSettings).expandX().fillX().colspan(4).bottom();

        settingsWindow.pack();
        settingsWindow.setPosition(screen.getWidth() / 2,
                screen.getHeight() / 2);

        settingsWindow.setScale(0f, 0f);
        settingsWindow.addAction(Actions.alpha(0f));
    }

    private void setUpCredits() {
        creditsWindow = new Window("CREDITS", new Window.WindowStyle(
                new BitmapFont(Gdx.files.internal("HUD/luckiestguy.fnt")),
                Color.WHITE,
                new NinePatchDrawable(new NinePatch(new Texture("HUD/panel_blue.png"), 7, 7, 7, 7))
        ));

        creditsWindow.padTop(64);
        creditsWindow.setKeepWithinStage(false);
        creditsWindow.getTitleLabel().setAlignment(Align.center);

        // Art and art credit
        Label art = new Label("ART", new Label.LabelStyle(
                new BitmapFont(Gdx.files.internal("HUD/luckiestguysmall.fnt")), Color.DARK_GRAY
        ));
        art.setAlignment(Align.center);

        Label artCredit1 = new Label("Kenney (kenney.nl)\n", new Label.LabelStyle(
                new BitmapFont(Gdx.files.internal("HUD/luckiestguyverysmall.fnt")), Color.LIGHT_GRAY
        ));

        artCredit1.setAlignment(Align.center);

        creditsWindow.add(art).fillX().expandX().row();
        creditsWindow.add(artCredit1).expandX().fillX().padLeft(10).padRight(10).row();

        // Music and music credit
        Label music = new Label("MUSIC", new Label.LabelStyle(
                new BitmapFont(Gdx.files.internal("HUD/luckiestguysmall.fnt")), Color.DARK_GRAY
        ));
        music.setAlignment(Align.center);

        Label musicCredit1 = new Label("Batty McFaddin Kevin MacLeod (incompetech.com)\n" +
                "Licensed under Creative Commons:\n" +
                "By Attribution 3.0 License\n" +
                "http://creativecommons.org/licenses/by/3.0/", new Label.LabelStyle(
                new BitmapFont(Gdx.files.internal("HUD/luckiestguyverysmall.fnt")), Color.LIGHT_GRAY
        ));


        musicCredit1.setAlignment(Align.center);

        creditsWindow.add(music).fillX().expandX().row();
        creditsWindow.add(musicCredit1).expandX().fillX().padLeft(10).padRight(10).padBottom(10).row();

        final TextButton dismissSettings = new TextButton("CLOSE", new TextButton.TextButtonStyle(
                new NinePatchDrawable(new NinePatch(new Texture("HUD/whiteUp.png"), 7, 7, 7, 9)),
                new NinePatchDrawable(new NinePatch(new Texture("HUD/whiteDown.png"), 7, 7, 10, 6)),
                new NinePatchDrawable(new NinePatch(new Texture("HUD/whiteDown.png"), 7, 7, 10, 6)),
                new BitmapFont(Gdx.files.internal("HUD/luckiestguymed.fnt"))
        ));

        dismissSettings.getLabel().setColor(Color.DARK_GRAY);

        dismissSettings.addListener(new ClickListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                dismissSettings.setChecked(true);

                return true;
            }

            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                super.touchUp(event, x, y, pointer, button);

                dismissSettings.setChecked(false);

                id = windowClose.play(.5f * screen.getSoundAmp());

                creditsWindow.addAction(Actions.sequence(
                        Actions.parallel(
                                Actions.moveBy(creditsWindow.getWidth() / 2, creditsWindow.getHeight() / 2, .3f, Interpolation.exp10In),
                                Actions.scaleTo(0f, 0f, .3f, Interpolation.exp10In),
                                Actions.fadeOut(.3f, Interpolation.fade)),
                        Actions.removeActor()));
            }
        });

        creditsWindow.add(dismissSettings).expandX().fillX().bottom();

        creditsWindow.pack();

        creditsWindow.setPosition(screen.getWidth() / 2,
                screen.getHeight() / 2);

        creditsWindow.setScale(0f, 0f);
        creditsWindow.addAction(Actions.alpha(0f));
    }

    private void setUpHowtoPlay() {
        howToPlayWindow = new Window("HOW TO PLAY", new Window.WindowStyle(
                new BitmapFont(Gdx.files.internal("HUD/luckiestguyborder.fnt")),
                Color.ORANGE,
                new NinePatchDrawable(new NinePatch(new Texture("HUD/yellow_panel.png"), 7, 7, 7, 7))
        ));
        howToPlayWindow.getTitleLabel().setAlignment(Align.center);
        howToPlayWindow.setKeepWithinStage(false);
        howToPlayWindow.padTop(64);
        howToPlayWindow.setMovable(false);


//                howToPlayWindow.setBounds(.2f * screen.getWidth(), .15f * screen.getHeight(),
//                        .6f * screen.getWidth(), .7f * screen.getHeight());

        // Add joystick help
        Image joystick = new Image(new Texture("HUD/joystick.png"));
        joystick.setColor(Color.MAGENTA);

        Label joystickHelp = new Label("Move cursor with joystick in lower left.\n" +
                "Fire with button in lower right.\n" +
                "Miss any shots and lose 5 seconds\n" +
                "Combos larger than 10 increase time.", new Label.LabelStyle(
                new BitmapFont(Gdx.files.internal("HUD/luckiestguysmall.fnt")), Color.FIREBRICK
        ));

        howToPlayWindow.add(joystick);
        howToPlayWindow.add(joystickHelp).fillX().expandX().padBottom(10).row();

        // Add target help
        Image target = new Image(new Texture("HUD/icon_target.png"));
        Label targetHelp = new Label("Shoot any target you see.\n" +
                "Faster targets net higher points.\n" +
                "Lose 5 seconds if any target gets away.", new Label.LabelStyle(
                new BitmapFont(Gdx.files.internal("HUD/luckiestguysmall.fnt")), Color.OLIVE
        ));

        howToPlayWindow.add(target);
        howToPlayWindow.add(targetHelp).fillX().expandX().padBottom(10).row();

        // Add duck help
        Image duck = new Image(new Texture("HUD/icon_duck.png"));
        Label duckHelp = new Label("Only shoot ducks with targets.\n" +
                "Faster ducks net higher points.\n" +
                "Don't shoot ducks without targets.", new Label.LabelStyle(
                new BitmapFont(Gdx.files.internal("HUD/luckiestguysmall.fnt")), Color.ROYAL
        ));

        howToPlayWindow.add(duck);
        howToPlayWindow.add(duckHelp).fillX().expandX().padBottom(10).row();

        final TextButton dismissSettings = new TextButton("CLOSE", new TextButton.TextButtonStyle(
                new NinePatchDrawable(new NinePatch(new Texture("HUD/fireUp.png"), 7, 7, 7, 9)),
                new NinePatchDrawable(new NinePatch(new Texture("HUD/fireDown.png"), 7, 7, 10, 6)),
                new NinePatchDrawable(new NinePatch(new Texture("HUD/fireDown.png"), 7, 7, 10, 6)),
                new BitmapFont(Gdx.files.internal("HUD/luckiestguymed.fnt"))
        ));

        dismissSettings.addListener(new ClickListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                dismissSettings.setChecked(true);

                return true;
            }

            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                super.touchUp(event, x, y, pointer, button);

                dismissSettings.setChecked(false);
                id = windowClose.play(.5f * screen.getSoundAmp());

                howToPlayWindow.addAction(Actions.sequence(
                        Actions.parallel(
                                Actions.moveBy(howToPlayWindow.getWidth() / 2, howToPlayWindow.getHeight() / 2, .3f, Interpolation.exp10In),
                                Actions.scaleTo(0f, 0f, .3f, Interpolation.exp10In),
                                Actions.fadeOut(.3f, Interpolation.fade)),
                        Actions.removeActor()));
            }
        });

        howToPlayWindow.add(dismissSettings).expandX().fillX().colspan(2).bottom();

        howToPlayWindow.pack();
        howToPlayWindow.setPosition(screen.getWidth() / 2,
                screen.getHeight() / 2);
        howToPlayWindow.setScale(0f, 0f);
        howToPlayWindow.addAction(Actions.alpha(0f));
    }

    private void addToStage() {
        stage.addActor(quick);
        stage.addActor(eye);
        stage.addActor(draw);
        stage.addActor(newGame);
        stage.addActor(settings);
        stage.addActor(howToPlay);
        stage.addActor(credits);
        stage.addActor(highScoreLabel);
        stage.addActor(highScoreVal);
        stage.addActor(support);
        stage.addActor(highComboLabel);
        stage.addActor(highComboVal);
        stage.addActor(exitButton);
        stage.addActor(supportDevLabel);
        stage.addActor(button1);
        stage.addActor(button3);
        stage.addActor(button7);
    }

    public void toFront() {
        quick.toFront();
        eye.toFront();
        draw.toFront();
        newGame.toFront();
        settings.toFront();
        howToPlay.toFront();
        credits.toFront();
        highScoreLabel.toFront();
        highScoreVal.toFront();
        highComboLabel.toFront();
        highComboVal.toFront();
        exitButton.toFront();
        supportDevLabel.toFront();
        button1.toFront();
        button3.toFront();
        button7.toFront();
        support.toFront();
        settingsWindow.toFront();
        howToPlayWindow.toFront();
        creditsWindow.toFront();
    }

    public void startMainMenu() {
//        Action fireAction = new Action() {
//            @Override
//            public boolean act(float delta) {
//                id = menuFire.play(screen.getSoundAmp() * .1f);
//                return true;
//            }
//        };

        Action playMusic = new Action() {
            @Override
            public boolean act(float delta) {
                menuMusic.play();
                menuMusic.setVolume(screen.getMusicAmp());
                return true;
            }
        };

        highScoreVal.remove();
        highComboVal.remove();

        resetHighScoreVal();
        resetHighComboVal();

        stage.addActor(highScoreVal);
        stage.addActor(highComboVal);

        quick.addAction(Actions.sequence(
                new Action() {
                    @Override
                    public boolean act(float delta) {
                        id = duckFire.play(screen.getSoundAmp());
                        return true;
                    }
                },
                Actions.addAction(Actions.moveTo(.3f * screen.getWidth() - quick.getWidth() / 2, quick.getY(), .5f, Interpolation.exp10Out), quick),
                Actions.delay(.2f),
                new Action() {
                    @Override
                    public boolean act(float delta) {
                        id = targetFire.play(screen.getSoundAmp());
                        return true;
                    }
                },
                Actions.addAction(Actions.moveTo(.5f * screen.getWidth() - eye.getWidth() / 2, eye.getY(), .5f, Interpolation.exp10Out), eye),
                Actions.delay(.2f),
                new Action() {
                    @Override
                    public boolean act(float delta) {
                        id = missFire.play(screen.getSoundAmp());
                        return true;
                    }
                },
                Actions.addAction(Actions.moveTo(.7f * screen.getWidth() - draw.getWidth() / 2, draw.getY(), .5f, Interpolation.exp10Out), draw),
                Actions.delay(.2f),
                Actions.parallel(
                        Actions.addAction(Actions.moveTo(screen.getWidth() / 2 - newGame.getWidth() / 2, newGame.getY(), .5f, Interpolation.exp10Out), newGame),
                        Actions.addAction(Actions.moveTo(screen.getWidth() / 2 - settings.getWidth() / 2, settings.getY(), .5f, Interpolation.exp10Out), settings),
                        Actions.addAction(Actions.moveTo(screen.getWidth() / 2 - howToPlay.getWidth() / 2, howToPlay.getY(), .5f, Interpolation.exp10Out), howToPlay),
                        Actions.addAction(Actions.moveTo(screen.getWidth() / 2 - credits.getWidth() / 2, credits.getY(), .5f, Interpolation.exp10Out), credits),
                        Actions.addAction(Actions.moveTo(screen.getWidth() - 15 - exitButton.getWidth(), exitButton.getY(), .5f, Interpolation.swingOut), exitButton),
                        playMusic
                ),
                Actions.delay(.2f),
                Actions.parallel(
                        Actions.addAction(Actions.moveTo(.75f * screen.getWidth(), highScoreLabel.getY(), .5f, Interpolation.exp5Out), highScoreLabel),
                        Actions.addAction(Actions.scaleTo(1f, 1f, .5f, Interpolation.circleIn), highScoreLabel),
                        Actions.addAction(Actions.moveTo(.72f * screen.getWidth(), highComboLabel.getY(), .5f, Interpolation.exp5Out), highComboLabel),
                        Actions.addAction(Actions.scaleTo(1f, 1f, .5f, Interpolation.circleIn), highComboLabel)
                ),
                Actions.delay(.3f),
                Actions.parallel(
                        Actions.addAction(Actions.moveTo(.75f * screen.getWidth() - (highScoreVal.getWidth() - highScoreLabel.getWidth()) / 2, highScoreVal.getY(), .5f, Interpolation.exp5Out), highScoreVal),
                        Actions.addAction(Actions.scaleTo(1f, 1f, .5f, Interpolation.circleIn), highScoreVal),
                        Actions.addAction(Actions.moveTo(.72f * screen.getWidth() - (highComboVal.getWidth() - highComboLabel.getWidth()) / 2, highComboVal.getY(), .5f, Interpolation.exp5Out), highComboVal),
                        Actions.addAction(Actions.scaleTo(1f, 1f, .5f, Interpolation.circleIn), highComboVal),
                        Actions.addAction(Actions.moveTo(.1f * screen.getWidth(), support.getY(), .5f, Interpolation.exp5Out), support),
                        Actions.addAction(Actions.scaleTo(1f, 1f, .5f, Interpolation.circleIn), support)
                )));

        highScoreLabel.addAction(Actions.after(
                Actions.forever(Actions.sequence(
                        Actions.rotateBy(10, .5f),
                        Actions.rotateBy(-10, .5f)
                ))));

        highScoreVal.addAction(Actions.after(
                Actions.forever(Actions.sequence(
                        Actions.rotateBy(10, .5f),
                        Actions.rotateBy(-10, .5f)
                ))));

        highComboLabel.addAction(Actions.after(
                Actions.forever(Actions.sequence(
                        Actions.rotateBy(10, .5f),
                        Actions.rotateBy(-10, .5f)
                ))));

        highComboVal.addAction(Actions.after(
                Actions.forever(Actions.sequence(
                        Actions.rotateBy(10, .5f),
                        Actions.rotateBy(-10, .5f)
                ))));

        support.addAction(Actions.after(Actions.forever(Actions.sequence(
                Actions.scaleTo(1.05f, 1.05f, .05f),
                Actions.scaleTo(1f, 1f, .05f),
                Actions.delay(.9f)
        ))));
    }

    public void leaveMainMenu() {
        Action stopMusic = new Action() {
            @Override
            public boolean act(float delta) {
                menuMusic.stop();
                return true;
            }
        };

        hideSupportButtons();

        Action fireAction = new Action() {
            @Override
            public boolean act(float delta) {
                id = menuFire.play(screen.getSoundAmp() * .1f);
                return true;
            }
        };

        stage.addAction(Actions.parallel(
                Actions.addAction(Actions.moveBy(-screen.getWidth(), 0, .5f, Interpolation.exp10Out), quick),
                Actions.addAction(Actions.moveBy(-screen.getWidth(), 0, .5f, Interpolation.exp10Out), eye),
                Actions.addAction(Actions.moveBy(-screen.getWidth(), 0, .5f, Interpolation.exp10Out), draw),
                Actions.addAction(Actions.moveBy(-screen.getWidth(), 0, .5f, Interpolation.exp10Out), newGame),
                Actions.addAction(Actions.moveBy(screen.getWidth(), 0, .5f, Interpolation.exp10Out), settings),
                Actions.addAction(Actions.moveBy(screen.getWidth(), 0, .5f, Interpolation.exp10Out), exitButton),
                Actions.addAction(Actions.moveBy(-screen.getWidth(), 0, .5f, Interpolation.exp10Out), howToPlay),
                Actions.addAction(Actions.moveBy(-screen.getWidth(), 0, .5f, Interpolation.exp10Out), credits),
                Actions.addAction(Actions.moveTo(screen.getWidth(), highScoreLabel.getY(), .25f), highScoreLabel),
                Actions.addAction(Actions.scaleTo(0f, 0f, .5f, Interpolation.exp10Out), highScoreLabel),
                Actions.addAction(Actions.moveTo(screen.getWidth(), highScoreVal.getY(), .25f), highScoreVal),
                Actions.addAction(Actions.scaleTo(0f, 0f, .5f, Interpolation.exp10Out), highScoreVal),
                Actions.addAction(Actions.moveTo(screen.getWidth(), highComboLabel.getY(), .25f), highComboLabel),
                Actions.addAction(Actions.scaleTo(0f, 0f, .5f, Interpolation.exp10Out), highComboLabel),
                Actions.addAction(Actions.moveTo(screen.getWidth(), highComboVal.getY(), .25f), highComboVal),
                Actions.addAction(Actions.scaleTo(0f, 0f, .5f, Interpolation.exp10Out), highComboVal),
                Actions.addAction(Actions.moveTo(-2*support.getWidth(), support.getY(), .25f), support),
                Actions.addAction(Actions.scaleTo(0f, 0f, .5f, Interpolation.exp10Out), support),
                stopMusic,
                fireAction));
    }

    public void updateCombo(int newMaxCombo) {
        highCombo = screen.getMaxCombo(newMaxCombo);
        highComboVal.setText("X " + String.valueOf(highCombo));
    }

    @Override
    public void dispose() {
//        menuFire.dispose();
//        menuMusic.dispose();
//        windowOpen.dispose();
//        windowClose.dispose();
    }

    public void updateScore(int score) {
        highScore = screen.getHighScore(score);
        highScoreVal.setText(String.valueOf(highScore));
    }

    public void toggleSupportButtons() {
        if (showingSupport)
            hideSupportButtons();
        else
            showSupportButtons();
    }

    private void showSupportButtons() {


        supportDevLabel.addAction(Actions.sequence(
                Actions.moveTo(support.getX() - 50, support.getY()),
                Actions.parallel(
                        Actions.scaleTo(1f, 1f, .25f, Interpolation.exp10Out),
                        Actions.moveBy(0, 130, .25f, Interpolation.exp10Out),
                        Actions.alpha(1f, .25f, Interpolation.fade)
                )
        ));

        button1.addAction(Actions.sequence(
                Actions.moveTo(support.getX() + 40, support.getY()),
                Actions.parallel(
                        Actions.scaleTo(1f, 1f, .25f, Interpolation.exp10Out),
                        Actions.moveBy(-100, -40, .25f, Interpolation.exp10Out),
                        Actions.alpha(1f, .25f, Interpolation.fade)
                )
        ));


        button3.addAction(Actions.sequence(
                Actions.moveTo(support.getX() + 40, support.getY()),
                Actions.parallel(
                        Actions.scaleTo(1f, 1f, .25f, Interpolation.exp10Out),
                        Actions.moveBy(0, -85, .25f, Interpolation.exp10Out),
                        Actions.alpha(1f, .25f, Interpolation.fade)
                )
        ));

        button7.addAction(Actions.sequence(
                Actions.moveTo(support.getX() + 40, support.getY()),
                Actions.parallel(
                        Actions.scaleTo(1f, 1f, .25f, Interpolation.exp10Out),
                        Actions.moveBy(100, -40, .25f, Interpolation.exp10Out),
                        Actions.alpha(1f, .25f, Interpolation.fade)
                )
        ));

        showingSupport = true;
    }

    private void hideSupportButtons() {

        supportDevLabel.addAction(Actions.sequence(
                Actions.parallel(
                        Actions.scaleTo(0f, 0f, .25f, Interpolation.exp10Out),
                        Actions.moveBy(0, -130, .25f, Interpolation.exp10Out),
                        Actions.alpha(0f, .25f, Interpolation.fade)),
                Actions.moveTo(-2*support.getWidth(), .35f * screen.getHeight())
        ));

        button1.addAction(Actions.sequence(
                Actions.parallel(
                        Actions.scaleTo(0f, 0f, .25f, Interpolation.exp10Out),
                        Actions.moveBy(100, 40, .25f, Interpolation.exp10Out),
                        Actions.alpha(0f, .25f, Interpolation.fade)),
                Actions.moveTo(-2*support.getWidth(), .35f * screen.getHeight())
        ));

        button3.addAction(Actions.sequence(
                Actions.parallel(
                        Actions.scaleTo(0f, 0f, .25f, Interpolation.exp10Out),
                        Actions.moveBy(0, 85, .25f, Interpolation.exp10Out),
                        Actions.alpha(0f, .25f, Interpolation.fade)),
                Actions.moveTo(-2*support.getWidth(), .35f * screen.getHeight())
        ));

        button7.addAction(Actions.sequence(
                Actions.parallel(
                        Actions.scaleTo(0f, 0f, .25f, Interpolation.exp10Out),
                        Actions.moveBy(-100, 40, .25f, Interpolation.exp10Out),
                        Actions.alpha(0f, .25f, Interpolation.fade)),
                Actions.moveTo(-2*support.getWidth(), .35f * screen.getHeight())
        ));

        showingSupport = false;
    }
}
