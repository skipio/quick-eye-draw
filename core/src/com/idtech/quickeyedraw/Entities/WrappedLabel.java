package com.idtech.quickeyedraw.Entities;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.utils.Align;

public class WrappedLabel extends Group {
    Label label;

    public WrappedLabel(String string, boolean isBig) {
        BitmapFont font;

        if (isBig) {
            font = new BitmapFont(Gdx.files.internal("HUD/luckiestguyborder.fnt"));
        } else {
            font = new BitmapFont(Gdx.files.internal("HUD/luckiestguybordersmall.fnt"));
        }

        label = new Label(string, new Label.LabelStyle(font, Color.WHITE));

        label.setOrigin(Align.center);

        addActor(label);

        this.setPosition(label.getX(), label.getY());
        this.setOrigin(Align.center);
    }

    @Override
    public float getWidth() {
//        return super.getWidth();
        return label.getWidth();
    }

    @Override
    public void setPosition(float x, float y) {
        super.setPosition(x, y);

//        label.setPosition(x, y);
    }

    @Override
    public float getHeight() {
//        return super.getHeight();
        return label.getHeight();
    }

    public void centerOrigin() {
//        setOrigin(getX() + getWidth() / 2, getY() + getHeight() / 2);
        label.setOrigin(Align.center);
        setOrigin(label.getOriginX(), label.getOriginY());
    }

    @Override
    public void setOrigin(float originX, float originY) {
        super.setOrigin(originX, originY);

        label.setOrigin(originX, originY);
    }

    public void fixWidth() {
        setWidth(label.getWidth());
    }

    public void setText(CharSequence newText) {
        label.setText(newText);
    }
}
