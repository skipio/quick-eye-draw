package com.idtech.quickeyedraw.Entities;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.NinePatchDrawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Timer;
import com.idtech.quickeyedraw.Screens.PlayScreen;

public class TimerLabel {
    Image[] timerBoard;

    PlayScreen screen;
    Stage stage;

    int initialTimer = 120;

    int timer = initialTimer;

    float[] timerPositions;

    float timerPositionY;

    ImageButton tryAgain;
    ImageButton share;
    ImageButton menu;
    Image gameOverImage;
    Image timeUpIm;
    Button pauseButton;
    WrappedLabel pauseLabel;

    boolean timerIsRunning;

    private Timer.Task clockTimer = new Timer.Task() {
        @Override
        public void run() {
            runTimer();
        }
    };

    public void startTimer() {
        if (!isTimerRunning()) {
            Timer.schedule(clockTimer, 1f, 1f);
            timerIsRunning = true;
        }
    }

    public boolean isTimerRunning() {
        return timerIsRunning;
    }

    private void runTimer() {
        timer--;
        if (timer <= 0) {
            clockTimer.cancel();
            timerIsRunning = false;
            screen.gameOver();
            timeUp();
        } else {
        }
        updateTimerBoard();
    }

    private void setUpExtraButtons() {
        tryAgain = new ImageButton(new ImageButton.ImageButtonStyle(
                new NinePatchDrawable(new NinePatch(new Texture("HUD/buttonUp.png"), 4, 4, 4, 8)),
                new NinePatchDrawable(new NinePatch(new Texture("HUD/buttonDown.png"), 4, 4, 8, 4)),
                new NinePatchDrawable(new NinePatch(new Texture("HUD/buttonDown.png"), 4, 4, 8, 4)),
                new TextureRegionDrawable(new TextureRegion(new Texture("HUD/return.png"))),
                new TextureRegionDrawable(new TextureRegion(new Texture("HUD/return.png"))),
                new TextureRegionDrawable(new TextureRegion(new Texture("HUD/return.png")))
        ));

        share = new ImageButton(new ImageButton.ImageButtonStyle(
                new NinePatchDrawable(new NinePatch(new Texture("HUD/buttonUp.png"), 4, 4, 4, 8)),
                new NinePatchDrawable(new NinePatch(new Texture("HUD/buttonDown.png"), 4, 4, 8, 4)),
                new NinePatchDrawable(new NinePatch(new Texture("HUD/buttonDown.png"), 4, 4, 8, 4)),
                new TextureRegionDrawable(new TextureRegion(new Texture("HUD/share.png"))),
                new TextureRegionDrawable(new TextureRegion(new Texture("HUD/share.png"))),
                new TextureRegionDrawable(new TextureRegion(new Texture("HUD/share.png")))
        ));

        menu = new ImageButton(new ImageButton.ImageButtonStyle(
                new NinePatchDrawable(new NinePatch(new Texture("HUD/buttonUp.png"), 4, 4, 4, 8)),
                new NinePatchDrawable(new NinePatch(new Texture("HUD/buttonDown.png"), 4, 4, 8, 4)),
                new NinePatchDrawable(new NinePatch(new Texture("HUD/buttonDown.png"), 4, 4, 8, 4)),
                new TextureRegionDrawable(new TextureRegion(new Texture("HUD/menu.png"))),
                new TextureRegionDrawable(new TextureRegion(new Texture("HUD/menu.png"))),
                new TextureRegionDrawable(new TextureRegion(new Texture("HUD/menu.png")))
        ));

        tryAgain.setPosition(- screen.getWidth(),
                .2f * screen.getHeight());
        share.setPosition(.5f * screen.getWidth() - share.getWidth() / 2,
                -screen.getHeight());
        menu.setPosition(2 * screen.getWidth(),
                .2f * screen.getHeight());

        tryAgain.addListener(new InputListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                tryAgain.setChecked(true);

                return true;
            }

            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                tryAgain.setChecked(false);
                screen.setGameSpeed(1f);
                pauseLabel.addAction(Actions.scaleTo(0f, 0f, .3f));
                screen.removeEntities();
                screen.restartGame();
            }
        });

        share.addListener(new InputListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                share.setChecked(true);

                return true;
            }

            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                share.setChecked(false);
                String message = "I just scored " + String.valueOf(screen.getScoreBoard().getScore())
                        + " on the crazy new game, Quick Eye Draw.\n\nTry it out on the Android Play store today for free here:\n" +
                        "http://bit.ly/qedgame";
                screen.getHandler().showShare(message);
            }
        });

        menu.addListener(new InputListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                menu.setChecked(true);

                return true;
            }

            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                menu.setChecked(false);
                pauseLabel.addAction(Actions.scaleTo(0f, 0f, .3f));
                screen.removeEntities();
                restartTimer();
                screen.toMenu();
            }
        });

        stage.addActor(tryAgain);
        stage.addActor(share);
        stage.addActor(menu);

        tryAgain.setOrigin(Align.center);
        tryAgain.addAction(Actions.color(Color.GREEN));

        share.setOrigin(Align.center);
        share.addAction(Actions.color(Color.BLUE));

        menu.setOrigin(Align.center);
        menu.addAction(Actions.color(Color.RED));

        gameOverImage = new Image(new Texture("HUD/text_gameover.png"));
        gameOverImage.setPosition(screen.getWidth() / 2 - gameOverImage.getWidth() / 2, .7f * screen.getHeight());
        gameOverImage.setScaleY(0);
        gameOverImage.setOrigin(Align.center);
        gameOverImage.setName("GAMEOVER");

        stage.addActor(gameOverImage);

        timeUpIm = new Image(new Texture("HUD/text_timeup.png"));
        timeUpIm.setPosition(screen.getWidth() / 2 - timeUpIm.getWidth() / 2, timerPositionY);
        timeUpIm.setOrigin(Align.center);
        timeUpIm.setScaleY(0);

        stage.addActor(timeUpIm);

        pauseButton = new Button(
                new TextureRegionDrawable(new TextureRegion(new Texture("HUD/pauseUp.png"))),
                new TextureRegionDrawable(new TextureRegion(new Texture("HUD/pauseDown.png")))
        );

        pauseButton.setPosition(10, screen.getHeight() - pauseButton.getHeight() - 10);
        pauseButton.setScale(.8f, .8f);

        pauseButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                if (screen.getPaused()) {
                    startTimer();
                    screen.returnControls();
                    hideExtraButtons();
                    screen.getTrigger().enableTrigger();
                    screen.getCurtains().open();
                    screen.resumeChain();
                    screen.setPaused(false);
                    pauseLabel.addAction(Actions.scaleTo(0f, 0f, .3f));
                } else {
                    clockTimer.cancel();
                    screen.clearControls();
                    showExtraButtons();
                    screen.getCurtains().close();
                    screen.pauseChain();
                    timerIsRunning = false;
                    screen.setPaused(true);
                    pauseLabel.addAction(Actions.scaleTo(1f, 1f, .3f));
                }
            }
        });

        stage.addActor(pauseButton);

        pauseLabel = new WrappedLabel("PAUSED", true);

        pauseLabel.setPosition(screen.getWidth() / 2 - pauseLabel.getWidth() / 2, .5f * screen.getHeight());
        pauseLabel.setScale(0f, 0f);
        pauseLabel.centerOrigin();
//        pauseLabel.setOrigin(screen.getWidth() / 2 - pauseLabel.getWidth() / 2,
//                screen.getHeight() / 2 - pauseLabel.getHeight() / 2);

        stage.addActor(pauseLabel);
    }

    private void timeUp() {
        pauseButton.setVisible(false);

        Action timeUpLabel = new Action() {
            @Override
            public boolean act(float delta) {

                timeUpIm.addAction(
                        Actions.parallel(
                                new Action() {
                                    @Override
                                    public boolean act(float delta) {
                                        screen.continueStage();
                                        return true;
                                    }
                                },
                                Actions.scaleTo(1f, 1f, .25f),
                                Actions.moveTo(screen.getWidth() / 2 - timeUpIm.getWidth() / 2, .7f * screen.getHeight(), 1f, Interpolation.fade)));

                Action gameOverGen = new Action() {
                    @Override
                    public boolean act(float delta) {



                        gameOverImage.addAction(Actions.scaleTo(1f, 1f, .2f));

                        showExtraButtons();

                        return true;
                    }
                };

                timeUpIm.addAction(Actions.after(
                        Actions.sequence(
                                Actions.scaleTo(1f, 0f, .2f),
                                gameOverGen
                        )
                ));

                return true;
            }
        };

        timerBoard[0].addAction(Actions.sequence(
                Actions.scaleTo(1f, 0, .25f),
                timeUpLabel));

        for (int i = 1, timerBoardLength = timerBoard.length; i < timerBoardLength; i++) {
            Image image = timerBoard[i];
            image.addAction(Actions.scaleTo(1f, 0, .25f));
        }
    }

    private void showExtraButtons() {
        tryAgain.addAction(Actions.moveTo(.3f * screen.getWidth() - tryAgain.getWidth() / 2,
                .2f * screen.getHeight(), 1f, Interpolation.exp10Out));
        share.addAction(Actions.moveTo(.5f * screen.getWidth() - share.getWidth() / 2,
                .2f * screen.getHeight(), 1f, Interpolation.exp10Out));
        menu.addAction(Actions.moveTo(.7f * screen.getWidth() - menu.getWidth() / 2,
                .2f * screen.getHeight(), 1f, Interpolation.exp10Out));
    }

    public void changeTime(int addedSecs) {
        timer += addedSecs;
        timer = MathUtils.clamp(timer, 1, 6039);
        updateTimerBoard();
    }

    public TimerLabel(PlayScreen screen, Stage stage) {
        timerIsRunning = false;

        this.screen = screen;
        this.stage = stage;

        timerBoard = new Image[5];

        timerPositions = new float[5];

        for (int i = 0, timerBoardLength = timerBoard.length; i < timerBoardLength; i++) {
            timerBoard[i] = new Image(new TextureRegionDrawable(new TextureRegion(screen.transparentTexture)));
//            timerBoard[i].setPosition((2 - i) * 48 + screen.getWidth() / 2, .9f * screen.getHeight());
            timerBoard[i].setY(.88f * screen.getHeight());
            timerBoard[i].setOrigin(Align.center);
        }

        timerPositionY = timerBoard[0].getY();

        ((TextureRegionDrawable)timerBoard[2].getDrawable()).getRegion().setTexture(screen.numTextures[11]);
        timerBoard[2].setSize(screen.numTextures[11].getWidth(), screen.numTextures[11].getHeight());
        timerBoard[2].setX(screen.getWidth() / 2 - timerBoard[2].getWidth() / 2);

        for (int i = 0, timerBoardLength = timerBoard.length; i < timerBoardLength; i++) {
            Image image = timerBoard[i];
            if (i < 2) {
                image.setX((i - 2) * 48 + timerBoard[2].getX());
            } else if ( i > 2) {
                image.setX((i-3)*48 + timerBoard[2].getX() + timerBoard[2].getWidth());
            }
            image.setOrigin(Align.center);

            stage.addActor(image);
            timerPositions[i] = image.getX();
        }
        updateTimerBoard();

        setUpExtraButtons();
        setVisibility(false);


    }

    public void setVisibility(boolean isVisible) {
        for (Image image: timerBoard) image.setVisible(isVisible);

        pauseButton.setDisabled(isVisible);
        pauseButton.setVisible(isVisible);
    }

    private void updateTimerBoard() {
        int min = timer / 60;
        int sec = timer % 60;
        String minVal = String.valueOf(min);
        String secVal = ("0" + String.valueOf(sec));
        secVal = secVal.substring(secVal.length() - 2);

        if (min < 10) {
            ((TextureRegionDrawable)timerBoard[0].getDrawable()).getRegion().setTexture(screen.transparentTexture);
            ((TextureRegionDrawable)timerBoard[1].getDrawable()).getRegion().setTexture(screen.numTextures[min]);
        } else {
            ((TextureRegionDrawable)timerBoard[0].getDrawable()).getRegion().
                    setTexture(screen.numTextures[Character.getNumericValue(minVal.charAt(1))]);
            ((TextureRegionDrawable)timerBoard[1].getDrawable()).getRegion().
                    setTexture(screen.numTextures[Character.getNumericValue(minVal.charAt(0))]);
        }

        ((TextureRegionDrawable)timerBoard[3].getDrawable()).getRegion().
                setTexture(screen.numTextures[Character.getNumericValue(secVal.charAt(0))]);
        ((TextureRegionDrawable)timerBoard[4].getDrawable()).getRegion().
                setTexture(screen.numTextures[Character.getNumericValue(secVal.charAt(1))]);

        for (int i = 0, timerBoardLength = timerBoard.length; i < timerBoardLength; i++) {
            Image image = timerBoard[i];
            Texture texture = ((TextureRegionDrawable) image.getDrawable()).getRegion().getTexture();
            image.setSize(texture.getWidth(), texture.getHeight());
            if (i != 2) {
                image.setX(timerPositions[i] + (48 - image.getWidth()) / 2);
            }
        }

    }

    public void restartTimer() {
        timer = initialTimer;
        updateTimerBoard();
    }

    public void toFront() {
        for (Image image:timerBoard) {
            image.toFront();
        }

        tryAgain.toFront();
        share.toFront();
        menu.toFront();
        timeUpIm.toFront();
        gameOverImage.toFront();

        pauseButton.toFront();

        pauseLabel.toFront();

    }

    public void gameStarted() {
        setVisibility(true);

        resetTimerLabel();

//        stage.addAction(Actions.parallel(
//                Actions.addAction(Actions.moveTo(-screen.getWidth(), .2f * screen.getHeight()), tryAgain),
//                Actions.addAction(Actions.moveTo(.5f * screen.getWidth() - share.getWidth() / 2, -screen.getHeight()), share),
//                Actions.addAction(Actions.moveTo(2 * screen.getWidth(), .2f * screen.getHeight()), menu),
//                returnTimer,
//                Actions.addAction(Actions.scaleTo(1f, 0f, .25f), gameOverImage)
//        ));


    }

    private void resetTimerLabel() {
        restartTimer();

        Action returnTimer = new Action() {
            @Override
            public boolean act(float delta) {


                return true;
            }
        };

        for(Image image:timerBoard) {
            image.addAction(Actions.scaleTo(1f, 1f, .25f));
        }

        gameOverImage.addAction(Actions.scaleTo(1f, 0f, .25f));

        hideExtraButtons();

//        tryAgain.addAction(
//                Actions.parallel(
//                        returnTimer,
//
//                )
//        );
    }

    private void hideExtraButtons() {
        tryAgain.addAction(Actions.moveTo(-screen.getWidth(), .2f * screen.getHeight(), .5f, Interpolation.pow4Out));
        share.addAction(Actions.moveTo(.5f * screen.getWidth() - share.getWidth() / 2, -screen.getHeight(), .5f, Interpolation.pow4Out));
        menu.addAction(Actions.moveTo(2 * screen.getWidth(), .2f * screen.getHeight(), .5f, Interpolation.pow4Out));
    }

    public void toMenu() {
        setVisibility(false);

        resetTimerLabel();
    }

    public int getTimer() {
        return timer;
    }
}
