package com.idtech.quickeyedraw.Entities;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.ImageTextButton;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Align;
import com.idtech.quickeyedraw.Screens.PlayScreen;

public class WrappedImageTextButton extends Group {
    private Button button;

    PlayScreen screen;

    public WrappedImageTextButton(final PlayScreen screen) {

        this.screen = screen;

        button = new Button(new Button.ButtonStyle(
                new TextureRegionDrawable(new TextureRegion(new Texture("HUD/heart.png"))),
                new TextureRegionDrawable(new TextureRegion(new Texture("HUD/heartDown.png"))),
                new TextureRegionDrawable(new TextureRegion(new Texture("HUD/heartDown.png")))
        ));

        button.addListener(new InputListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int b) {
                button.setChecked(true);

                return true;
            }

            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int b) {
                super.touchUp(event, x, y, pointer, b);

                button.setChecked(false);

                screen.getMenu().toggleSupportButtons();

            }
        });

        button.setOrigin(Align.center);

        addActor(button);

        this.setPosition(button.getX(), button.getY());
        this.setOrigin(Align.center);
    }

    @Override
    public float getWidth() {
        return button.getWidth();
    }

    @Override
    public float getHeight() {
        return button.getHeight();
    }

    public void centerOrigin() {
        setOrigin(button.getOriginX(), button.getOriginY());
    }


}
