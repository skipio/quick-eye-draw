package com.idtech.quickeyedraw.Entities;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.NinePatchDrawable;
import com.badlogic.gdx.utils.Disposable;
import com.idtech.quickeyedraw.Screens.PlayScreen;

public class Trigger extends TextButton implements Disposable {
//    static TextButtonStyle style = new TextButtonStyle(
//            new NinePatchDrawable(new NinePatch(new Texture("HUD/fireUp.png"), 7, 7, 7, 9)),
//            new NinePatchDrawable(new NinePatch(new Texture("HUD/fireDown.png"), 7, 7, 10, 6)),
//            new NinePatchDrawable(new NinePatch(new Texture("HUD/fireDown.png"), 7, 7, 10, 6)),
//            new BitmapFont(Gdx.files.internal("HUD/luckiestguy.fnt"))
//    );

    Sound gunFire;

    PlayScreen screen;


    public Trigger(final PlayScreen screen, Stage stage) {
        super("FIRE!", new TextButtonStyle(
                new NinePatchDrawable(new NinePatch(new Texture("HUD/fireUp.png"), 7, 7, 7, 9)),
                new NinePatchDrawable(new NinePatch(new Texture("HUD/fireDown.png"), 7, 7, 10, 6)),
                new NinePatchDrawable(new NinePatch(new Texture("HUD/fireDown.png"), 7, 7, 10, 6)),
                new BitmapFont(Gdx.files.internal("HUD/luckiestguy.fnt"))
        ));

        this.screen = screen;

//        gunFire = Gdx.audio.newSound(Gdx.files.internal("Sounds/bang.wav"));
        gunFire = screen.getManager().get("Sounds/bang.mp3");

        final long[] id = new long[1];

        float margin = 20;

        setPosition(screen.getWidth() - getWidth() - margin , margin);

        addAction(Actions.color(Color.RED));

        addListener(new InputListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                setChecked(true);
                id[0] = gunFire.play(screen.getSoundAmp() * 1.0f);
                screen.fire();
                return true;
            }

            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                super.touchUp(event, x, y, pointer, button);

                setChecked(false);
            }
        });

        setTouchable(Touchable.disabled);
        addAction(Actions.alpha(0f));

        stage.addActor(this);
    }

    public void gameOver() {
        setTouchable(Touchable.disabled);
        addAction(Actions.fadeOut(.5f));
    }

    public void gameStarted() {
//        setTouchable(Touchable.enabled);
        addAction(Actions.fadeIn(.5f));

    }

    public void enableTrigger() {
        setTouchable(Touchable.enabled);
    }

    @Override
    public void dispose() {
//        gunFire.dispose();
    }
}
