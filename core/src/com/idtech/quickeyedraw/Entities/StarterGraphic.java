package com.idtech.quickeyedraw.Entities;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Align;
import com.idtech.quickeyedraw.Screens.PlayScreen;

public class StarterGraphic extends Image {
    PlayScreen screen;

    boolean notScaled, inverted;

    float originalX;

    Stage stage;

    Image go;

    float startDuration = 2f;


    public StarterGraphic(final PlayScreen screen, Stage stage) {
        super(new Texture("HUD/text_ready.png"));
        this.screen = screen;

        this.stage = stage;
        setOrigin(Align.center);

        setPosition(screen.getWidth() / 2 - getWidth() / 2, screen.getHeight());


        originalX = getX();

//        Action startGameAction = new Action() {
//            @Override
//            public boolean act(float delta) {
//                screen.open();
//                return true;
//            }
//        };

        go = new Image(new Texture("HUD/text_go.png"));
        go.setOrigin(Align.center);
        go.setPosition(screen.getWidth() / 2 - go.getWidth() / 2,
                screen.getHeight() / 2 - go.getHeight() / 2);
        go.setScaleY(0);

        stage.addActor(this);
        stage.addActor(go);
    }

    public void startStarter() {
        addAction(Actions.sequence(
                Actions.parallel(Actions.moveTo(screen.getWidth() / 2 - getWidth() / 2,
                        screen.getHeight() / 2 - getHeight() / 2,
                        startDuration, Interpolation.pow4),
                        Actions.sequence(
                                Actions.delay(startDuration - .4f),
                                Actions.scaleTo(1f, 0, .4f, Interpolation.circleIn))),
                new Action() {
                    @Override
                    public boolean act(float delta) {
                        screen.startGame();
                        return true;
                    }
                },
                Actions.addAction(Actions.sequence(
                        Actions.scaleTo(1f, 1f, .4f, Interpolation.circleOut),
                        Actions.moveTo(screen.getWidth() / 2 - go.getWidth() / 2,
                                screen.getHeight(), .5f, Interpolation.exp10In)), go)));

        go.setPosition(screen.getWidth() / 2 - go.getWidth() / 2,
                screen.getHeight() / 2 - go.getHeight() / 2);
        go.setScaleY(0);

        setPosition(screen.getWidth() / 2 - getWidth() / 2, screen.getHeight());
        setScaleY(1f);

    }

    @Override
    public void toFront() {
        super.toFront();

        go.toFront();
    }
}
