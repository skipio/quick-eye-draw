package com.idtech.quickeyedraw.Entities;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.NinePatchDrawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Align;
import com.idtech.quickeyedraw.Screens.PlayScreen;

public class SupportButton extends Group {
    PlayScreen screen;

    TextButton button;

    public SupportButton(final PlayScreen screen, String cost, final String productIdName) {
        this.screen = screen;

        BitmapFont font = new BitmapFont(Gdx.files.internal("HUD/luckiestguyborder.fnt"));

        button = new TextButton("£" + cost, new TextButton.TextButtonStyle(
                new NinePatchDrawable(new NinePatch(screen.transparentTexture)),
                new NinePatchDrawable(new NinePatch(screen.transparentTexture)),
                new NinePatchDrawable(new NinePatch(screen.transparentTexture)),
                font
        ));


        addActor(button);

        button.addListener(new InputListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                screen.getHandler().showShare("SUPPORT" + productIdName);
                return true;
            }
        });

        this.setPosition(button.getX(), button.getY());
        centerOrigin();

    }

    public void centerOrigin() {
        button.setOrigin(Align.center);
        setOrigin(button.getOriginX(), button.getOriginY());
    }
}
