package com.idtech.quickeyedraw.Entities;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Disposable;
import com.idtech.quickeyedraw.Screens.PlayScreen;

public class Target extends Image implements Disposable {

    PlayScreen screen;
    Image stickImage;

    int score;

    float distanceMoved;

    float positionX, positionY;

    float targetTime;

    float animatedeadspeed = -5f;

    boolean hitByPlayer;

    Circle circle;

    boolean hit, changedTexture;


//    private static Texture targetBackOutline = new Texture("Objects/target_back_outline.png");

//    private static Texture metalStick = new Texture("Objects/stick_metal_outline.png");

//    private static Texture[] targetArray = {
//            new Texture("Objects/target_red3_outline.png"),
//            new Texture("Objects/target_red2_outline.png"),
//            new Texture("Objects/target_red1_outline.png"),
//            new Texture("Objects/target_colored_outline.png"),
//            new Texture("Objects/target_white_outline.png")
//    };

    public Target(PlayScreen screen, int type) {
        super(new Texture("Objects/target" + String.valueOf(type) + ".png"));

        this.screen = screen;

        hitByPlayer = false;

        score = 30 * (type + 1);

        distanceMoved = 5 * type;

//        positionX = screen.getRandomGen().nextFloat() * screen.getWidth() * .3f;
        positionX = screen.getWidth() * (screen.getRandomGen().nextFloat() / 2 + .1f);
        positionY = .2f * screen.getHeight();

        setPosition(positionX, positionY);

        targetTime = 0;

        hit = false;
        changedTexture = false;

        stickImage = new Image(new Texture("Objects/stick_metal_outline.png"));
        updateStickImagePosition();

        circle = new Circle();
//        circle.set(new Vector2(getX() + getWidth() / 2, getY() + getHeight() / 2), getWidth() / 2);
        circle.setRadius(getWidth() / 2);

        updateCirclePosition();
        screen.getStage().addActor(this);
        screen.getStage().addActor(stickImage);
    }

    @Override
    public void act(float delta) {

        if (!hit) {
            targetTime += delta;

            float changeX = MathUtils.sin(targetTime * 5) * distanceMoved * screen.getGameSpeed();
            float changeY = MathUtils.sin(targetTime) * 1.7f;

            moveBy(changeX, changeY);
            updateStickImagePosition();
            updateCirclePosition();

            if (targetTime > 2 * MathUtils.PI) dispose();
        } else {
            if (getScaleX() > 0) {
                animateDead();
            } else {
                if (!changedTexture) {
                    setDrawable(new TextureRegionDrawable(new TextureRegion(new Texture("Objects/target_back_outline.png"))));
                    changedTexture = true;
                }
                if (getScaleX() <= -1) {
                    if (getY() > 0) {
                        moveBy(0, animatedeadspeed);
                        stickImage.moveBy(0, animatedeadspeed);
                        animatedeadspeed -= .5f;
                    } else {
                        remove();
                        stickImage.remove();
                        return;
                    }

                } else {
                    animateDead();
                }

            }
        }
    }

    private void animateDead() {
        float oldWidth = getScaleX() * getWidth();
        scaleBy(-.25f, 0f);
        setScaleX(MathUtils.clamp(getScaleX(), -1, 1));
        float newWidth = getScaleX() * getWidth();
        moveBy((oldWidth - newWidth) / 2, 0f);
    }

    public Circle getBounds() {
        return circle;
    }

    private void updateStickImagePosition() {
        stickImage.setPosition(getX() + getWidth() / 2 - stickImage.getWidth() / 2,
                getY() - stickImage.getHeight());
    }

    public void setHitByPlayer() {
        hitByPlayer = true;
    }

    @Override
    public void dispose() {
        hit = true;
        circle.setPosition(-400, -400);

        if (!hitByPlayer && !screen.getGameOverState() && !screen.timerOver()) {
            screen.changeTimer(-3);
            screen.timeLossPopup();
        }
//        remove();
//        stickImage.remove();
    }


    public void quietDispose() {
        hit = true;
        circle.setPosition(-400, -400);

    }

    private void updateCirclePosition() {
        circle.setPosition(getX() + getWidth() / 2, getY() + getHeight() / 2);
    }

    public boolean offScreen() {
        return  getX() > screen.getWidth();
    }


    public void fixStick() {
        stickImage.toFront();
    }

    public int getScore() {
        return  score;
    }


}
