package com.idtech.quickeyedraw.Screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.RandomXS128;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.idtech.quickeyedraw.Entities.Crosshair;
import com.idtech.quickeyedraw.Entities.Curtains;
import com.idtech.quickeyedraw.Entities.DPad;
import com.idtech.quickeyedraw.Entities.Duck;
import com.idtech.quickeyedraw.Entities.MainMenu;
import com.idtech.quickeyedraw.Entities.PopupScore;
import com.idtech.quickeyedraw.Entities.ScoreBoard;
import com.idtech.quickeyedraw.Entities.StarterGraphic;
import com.idtech.quickeyedraw.Entities.Target;
import com.idtech.quickeyedraw.Entities.TimerLabel;
import com.idtech.quickeyedraw.Entities.Trigger;
import com.idtech.quickeyedraw.QEDGame;
import com.idtech.quickeyedraw.Tools.CommHandler;

public class PlayScreen implements Screen {
    private QEDGame game;
    private Viewport viewport;
    private OrthographicCamera gamecam;

    Preferences gameData;

    Sound hitTarget, hitDuck, failhit;
    Music chain;
    long targetId, duckId, failId, chainId;

    int maxCombo, highScore;

    float gameSpeed;

    float soundAmp, musicAmp, cursorSensitivity;

    public Texture[] numTextures = {
            new Texture("HUD/text_0.png"),
            new Texture("HUD/text_1.png"),
            new Texture("HUD/text_2.png"),
            new Texture("HUD/text_3.png"),
            new Texture("HUD/text_4.png"),
            new Texture("HUD/text_5.png"),
            new Texture("HUD/text_6.png"),
            new Texture("HUD/text_7.png"),
            new Texture("HUD/text_8.png"),
            new Texture("HUD/text_9.png"),
            new Texture("HUD/text_cross.png"),
            new Texture("HUD/text_dots.png"),
            new Texture("HUD/text_plus.png"),
            new Texture("HUD/text_minus.png")
    };

    public Texture transparentTexture = new Texture("HUD/transparent.png");

    float duckTimer, targetTimer;

    private RandomXS128 randomGen;

    private Stage stage;
    boolean isPaused;

    Image background;
    Image grass;
    Image curtainStriped;
    Image backWave;
    Image frontWave;
    Image frontdeck;

    Crosshair crosshair;
    Trigger trigger;

    DPad dPad;
    ScoreBoard scoreBoard;

    TimerLabel timerLabel;

    MainMenu menu;

    Curtains curtains;

    Texture backWaveTexture;
    Texture frontWaveTexture;

    int frontwaveX, maxFrontWaveX;
    int backwaveX, maxBackWaveX;

    StarterGraphic starterGraphic;

    ShapeRenderer renderer;

    private Array<Duck> duckArray;
    private Array<Target> targetArray;
    private boolean gameOverState;

    CommHandler handler;

    AssetManager manager;

    public PlayScreen(QEDGame game, AssetManager manager, CommHandler handler) {
        this.game = game;

        this.manager = manager;
        this.handler = handler;

        randomGen = new RandomXS128();

        gamecam = new OrthographicCamera();
        viewport = new FitViewport(QEDGame.V_WIDTH, QEDGame.V_HEIGHT, gamecam);
        gamecam.position.set(viewport.getWorldWidth() / 2, viewport.getWorldHeight() / 2, 0);

        stage = new Stage(viewport);

        isPaused = false;

        duckArray = new Array<Duck>();
        targetArray = new Array<Target>();

//        hitTarget = Gdx.audio.newSound(Gdx.files.internal("Sounds/hitTarget.wav"));
//        hitDuck = Gdx.audio.newSound(Gdx.files.internal("Sounds/hitDuck.mp3"));
//        failhit = Gdx.audio.newSound(Gdx.files.internal("Sounds/failhit.ogg"));
//        chain = Gdx.audio.newSound(Gdx.files.internal("Sounds/chain_loop.mp3"));

        hitTarget = manager.get("Sounds/hitTarget.wav");
        hitDuck = manager.get("Sounds/hitDuck.mp3");
        failhit = manager.get("Sounds/failhit.ogg");
        chain = manager.get("Sounds/chain_loop.mp3");

        duckTimer = .95f;
        targetTimer = .95f;

        gameData = Gdx.app.getPreferences("Game Data");

        gameOverState = true;

        soundAmp = gameData.getFloat("soundAmp", .6f);
        musicAmp = gameData.getFloat("musicAmp", .6f);
        cursorSensitivity = gameData.getFloat("sensitivity", 60f);

        gameSpeed = 1f;

        maxCombo = gameData.getInteger("maxCombo", 0);
        highScore = gameData.getInteger("highScore", 0);

        setupStage();

        setPaused(true);

        renderer = new ShapeRenderer();
    }

    private void setupStage() {


        Texture wood = new Texture("Stall/bg_wood.png");
        background = repeatImage(wood,
                false, 0);


        Texture grassTexture = new Texture("Stall/grass3.png");
        grass = repeatImage(grassTexture,
                true, .15f * getHeight());


        Texture curtainStripedTexture = new Texture("Stall/curtain_straight.png");
        curtainStriped = repeatImage(curtainStripedTexture,
                true, getHeight() - curtainStripedTexture.getHeight());

        backWaveTexture = new Texture("Stall/water1.png");
        backwaveX = 0;
        maxBackWaveX = backWaveTexture.getWidth();
        backWave = repeatImage(backWaveTexture, true, backwaveX);

        frontWaveTexture = new Texture("Stall/water2.png");
        frontWave = repeatImage(frontWaveTexture, true, -25);
        frontwaveX = frontWaveTexture.getWidth() / 2;
        maxFrontWaveX = frontWaveTexture.getWidth();
        ((TextureRegionDrawable)frontWave.getDrawable()).getRegion().setRegionX(frontwaveX);

        Texture frontwood = new Texture("Stall/frontwood.png");
        frontdeck = new Image(frontwood);
        frontdeck.setBounds(0, 0, getWidth(), frontwood.getHeight());

        dPad = new DPad(this, game.batch);

        scoreBoard = new ScoreBoard(this, dPad.getStage());
//        scoreBoard.toStage();

        starterGraphic = new StarterGraphic(this, dPad.getStage());

        timerLabel = new TimerLabel(this, dPad.getStage());

        menu = new MainMenu(this, dPad.getStage());
        menu.startMainMenu();

        crosshair = new Crosshair(this, dPad.getStage());
        trigger = new Trigger(this, dPad.getStage());
        dPad.getStage().addActor(curtainStriped);

        stage.addActor(background);
        stage.addActor(grass);
        stage.addActor(backWave);
        stage.addActor(frontWave);
        stage.addActor(frontdeck);
//        stage.addActor(crosshair);

        curtains = new Curtains(this, dPad.getStage());
    }

    private Image repeatImage(Texture texture, boolean singleHeight, float positionY) {
        texture.setWrap(Texture.TextureWrap.Repeat, Texture.TextureWrap.Repeat);
        TextureRegion repeatRegion = new TextureRegion(texture,
                0, 0, getWidth(), getHeight());
        if (singleHeight) repeatRegion.setRegionHeight(texture.getHeight());

        Image repeat = new Image(repeatRegion);
        repeat.setPosition(0, positionY);

        return repeat;
    }

    @Override
    public void show() {

    }

    @Override
    public void render(float delta) {
        update(delta);

        Gdx.gl.glClearColor(0.765625f, 0.52734375f, 0.3125f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        indexing();
        stage.draw();

        game.batch.setProjectionMatrix(gamecam.combined);
//        game.batch.begin();
//        game.batch.end();

        game.batch.setProjectionMatrix(dPad.getStage().getCamera().combined);
        dPad.getStage().draw();

//        renderer.setProjectionMatrix(stage.getCamera().combined);
//        renderer.begin(ShapeRenderer.ShapeType.Filled);
//        renderer.setColor(Color.RED);
//
//        for (Target target: targetArray) {
//            Circle targetCircle = target.getBounds();
//            renderer.circle(targetCircle.x, targetCircle.y, targetCircle.radius);
//        }
//
//        for (Duck duck: duckArray) {
//            Circle targetCircle = duck.getBounds();
//            renderer.circle(targetCircle.x, targetCircle.y, targetCircle.radius);
//        }
//
//        Circle crosshairCircle = crosshair.getBounds();
//        renderer.circle(crosshairCircle.x, crosshairCircle.y, crosshairCircle.radius);
//
//        renderer.end();
    }

    private void update(float delta) {
        if (!isPaused) {

            if (duckTimer > 3) {
                duckTimer = 0f;
                duckArray.add(new Duck(this, randomGen.nextInt(3), randomGen.nextInt(2)));
            }

            duckTimer += delta;

            if (targetTimer > 2) {
                targetTimer = 0f;
                targetArray.add(new Target(this, randomGen.nextInt(5)));
            }

            targetTimer += delta;

            if (frontwaveX > maxFrontWaveX) frontwaveX = 0;
            ((TextureRegionDrawable)frontWave.getDrawable()).getRegion().setRegionX(++frontwaveX);

            if (backwaveX < -maxBackWaveX) backwaveX = 0;
            ((TextureRegionDrawable)backWave.getDrawable()).getRegion().setRegionX(--backwaveX);

            stage.act(delta);
        }

        dPad.getStage().act(delta);

        gamecam.update();
    }

    public void continueStage() {
        stage.act(Gdx.graphics.getDeltaTime());
    }

    private void indexing() {
//        background.setZIndex(0);
        fixTargetIndex();
        grass.toFront();
        backWave.toFront();
        fixDuckIndex();
        frontWave.toFront();
        frontdeck.toFront();
        curtains.toFront();
        curtainStriped.toFront();
        crosshair.toFront();

        menu.toFront();

        trigger.toFront();
        dPad.toFront();

        scoreBoard.toFront();
        timerLabel.toFront();

        starterGraphic.toFront();
    }

    private void fixDuckIndex() {
        for (Duck duck: duckArray) {
            duck.toFront();
            duck.fixStick();
        }
    }

    private void fixTargetIndex() {
        for (Target target: targetArray) {
            target.toFront();
            target.fixStick();
        }
    }

    @Override
    public void resize(int width, int height) {
        viewport.update(width, height);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    public Curtains getCurtains() {
        return curtains;
    }

    public float getHeight() {
        return viewport.getWorldHeight();
    }

    public float getWidth() {
        return viewport.getWorldWidth();
    }

    public Stage getStage() {
        return stage;
    }

    public RandomXS128 getRandomGen() {
        return randomGen;
    }

    public void setPaused(boolean pauseState) {
        isPaused = pauseState;
    }

    public void moveCrossHair(float delX, float delY) {
        float mul = 8f * cursorSensitivity / 100;

        crosshair.moveBy(delX * mul, delY * mul);

        if (crosshair.getX() < 0) crosshair.setX(0);

        if (crosshair.getX() > getWidth() - crosshair.getWidth())
            crosshair.setX(getWidth() - crosshair.getWidth());

        if (crosshair.getY() < 0) crosshair.setY(0);

        if (crosshair.getY() > getHeight() - crosshair.getHeight())
            crosshair.setY(getHeight() - crosshair.getHeight());
    }

    public void fire() {
        int targetIndex = -1;
        Target disposedTarget = null;
        for (Target target : targetArray) {
            if (target.getBounds().overlaps(crosshair.getBounds())) {

                if (targetIndex < target.getZIndex()) {
                    targetIndex = target.getZIndex();
                    disposedTarget = target;
                }
//                System.out.println(target.getZIndex());
//                target.dispose();
//                break;
            }
        }
        int duckIndex = -1;
        Duck disposedDuck = null;
        for (Duck duck : duckArray) {
            if (duck.getBounds().overlaps(crosshair.getBounds())) {

//                System.out.println(duck.getZIndex());
//                duck.dispose();
//                break;
                if (duckIndex < duck.getZIndex()) {
                    duckIndex = duck.getZIndex();
                    disposedDuck = duck;
                }
            }
        }

        int scoreVal = 0;

        if (targetIndex > duckIndex && disposedTarget != null) {
            scoreVal = disposedTarget.getScore();
            disposedTarget.setHitByPlayer();
            targetId = hitTarget.play(soundAmp * 1.0f);
            scoreBoard.addScore(disposedTarget.getScore());
            disposedTarget.dispose();
            scoreBoard.updateComboScore(true);
            gameSpeed += .05f;
        } else if (duckIndex > targetIndex && disposedDuck != null) {
            scoreVal = disposedDuck.getScore();
            disposedDuck.setHitByPlayer();
            duckId = hitDuck.play(soundAmp * 1.0f);
            scoreBoard.addScore(disposedDuck.getScore());
            disposedDuck.dispose();
            gameSpeed += .05f;
            scoreBoard.updateComboScore(true);
        } else {
            scoreBoard.updateComboScore(false);
        }

        gameSpeed = Math.min(gameSpeed, 4.0f);

        if (scoreVal != 0)
            new PopupScore(getDpadStage(), scoreVal,
                    crosshair.getX() + crosshair.getWidth() / 2,
                    crosshair.getY() + crosshair.getHeight());
        else {
            timerLabel.changeTime(-5);
            new PopupScore(getDpadStage(),
                    crosshair.getX() + crosshair.getWidth() / 2,
                    crosshair.getY() + crosshair.getHeight());
        }

        scoreBoard.updateScoreBoard();

    }

    public void gameOver() {
        gameOverState = true;
        removeEntities();

        curtains.close();
        scoreBoard.gameOver();
        clearControls();

        isPaused = true;

        pauseChain();

    }

    public void pauseChain() {
        chain.stop();
    }

    public void resumeChain() {
        chain.play();
        chain.setVolume(1.0f * soundAmp);
        chain.setLooping(true);
    }

    public void removeEntities() {
        for (Duck duck: duckArray)
            duck.quietDispose();
        duckArray.clear();
        for (Target target: targetArray)
            target.quietDispose();
        targetArray.clear();
    }

    public void clearControls() {
        crosshair.gameOver();
        dPad.gameOver();
        trigger.gameOver();
    }

    public void startGame() {
        isPaused = false;
        trigger.enableTrigger();
        curtains.open();
        timerLabel.startTimer();
        gameOverState = false;
        resumeChain();
    }

    public void newGame() {
        gameSpeed = 1f;
        starterGraphic.startStarter();
        returnControls();
        scoreBoard.gameStarted();
        timerLabel.gameStarted();
    }

    public void returnControls() {
        crosshair.gameStarted();
        dPad.gameStarted();
        trigger.gameStarted();
    }

    public void restartGame() {
        scoreBoard.refreshScore();
//        starterGraphic.startStarter();
        starterGraphic.startStarter();
        scoreBoard.gameStarted();
        timerLabel.restartTimer();
        timerLabel.gameStarted();

        returnControls();

        duckTimer = .95f;
        targetTimer = .95f;
    }

    public void toMenu() {
        if (!gameOverState)
            gameOver();

        timerLabel.toMenu();
        menu.startMainMenu();
        scoreBoard.toMenu();
    }

    public void changeTimer(int addedTime) {
        timerLabel.changeTime(addedTime);
    }

    public Stage getDpadStage() {
        return dPad.getStage();
    }

    public MainMenu getMenu() {
        return menu;
    }

    public TimerLabel getTimer() {
        return timerLabel;
    }

    public float getSoundAmp() {
        return soundAmp;
    }

    public void timeLossPopup() {
//        new PopupScore(getDpadStage(), .7f * getWidth(), .88f * getHeight());
        failId = failhit.play(soundAmp * 1.0f);

        new PopupScore(getDpadStage(),
                crosshair.getX() + crosshair.getWidth() / 2,
                crosshair.getY() + crosshair.getHeight());
    }

    public Trigger getTrigger() {
        return trigger;
    }

    public float getMusicAmp() {
        return musicAmp;
    }

    public float getCursorSensitivity() {
        return cursorSensitivity;
    }

    public void setSoundAmp(float soundAmp) {
        this.soundAmp = soundAmp;
        gameData.putFloat("soundAmp", soundAmp);
        gameData.flush();
    }

    public void setMusicAmp(float musicAmp) {
        this.musicAmp = musicAmp;
        gameData.putFloat("musicAmp", musicAmp);
        gameData.flush();
    }

    public void setCursorSensitivity(float cursorSensitivity) {
        this.cursorSensitivity = cursorSensitivity;
        gameData.putFloat("sensitivity", cursorSensitivity);
        gameData.flush();
    }

    public boolean getPaused() {
        return isPaused;
    }

    public int getHighScore(int score) {
        highScore = Math.max(highScore, score);
        gameData.putInteger("highScore", highScore);
        gameData.flush();
        return highScore;
    }

    public int getMaxCombo(int newMaxCombo) {
        maxCombo = Math.max(maxCombo, newMaxCombo);
        gameData.putInteger("maxCombo", maxCombo);
        gameData.flush();
        return maxCombo;
    }

    public float getGameSpeed() {
        return gameSpeed;
    }

    @Override
    public void dispose() {
        stage.dispose();
        renderer.dispose();
        menu.dispose();
        trigger.dispose();
//        failhit.dispose();
//        hitTarget.dispose();
//        hitDuck.dispose();
//        chain.dispose();
        scoreBoard.dispose();
    }

    public void setGameSpeed(float speed) {
        gameSpeed = speed;
    }

    public boolean getGameOverState() {
        return gameOverState;
    }

    public int getPresentHighScore() {
        return highScore;
    }

    public int getPresentMaxCombo() {
        return maxCombo;
    }

    public AssetManager getManager() {
        return manager;
    }

    public boolean timerOver() {
        return timerLabel.getTimer() < 3;
    }

    public CommHandler getHandler() {
        return handler;
    }

    public ScoreBoard getScoreBoard() {
        return scoreBoard;
    }
}
