package com.idtech.quickeyedraw;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.idtech.quickeyedraw.Screens.PlayScreen;
import com.idtech.quickeyedraw.Tools.CommHandler;

public class QEDGame extends Game {
	public static final int V_WIDTH = 1920 / 2;
	public static final int V_HEIGHT = 1080 / 2;
//	public static final float PPM = 100;
	public AssetManager manager;
	CommHandler handler;

	public SpriteBatch batch;

	public QEDGame(CommHandler handler) {
		this.handler = handler;
	}

	@Override
	public void create () {
		batch = new SpriteBatch();
		manager = new AssetManager();
		manager.load("Music/Batty McFaddin.mp3", Music.class);
		manager.load("Music/sillycircus.ogg", Music.class);
		manager.load("Sounds/bang.mp3", Sound.class);
		manager.load("Sounds/chain_loop.mp3", Music.class);
		manager.load("Sounds/failhit.ogg", Sound.class);
		manager.load("Sounds/gunfire.wav", Sound.class);
		manager.load("Sounds/hitDuck.mp3", Sound.class);
		manager.load("Sounds/hitTarget.wav", Sound.class);
		manager.load("Sounds/tadaa.wav", Sound.class);
		manager.load("Sounds/windowClose.ogg", Sound.class);
		manager.load("Sounds/windowOpen.ogg", Sound.class);
		manager.finishLoading();
		setScreen(new PlayScreen(this, manager, handler));
	}

	@Override
	public void render () {
		super.render();
		manager.update();

	}
	
	@Override
	public void dispose () {
		super.dispose();
		batch.dispose();
		manager.dispose();
	}
}
